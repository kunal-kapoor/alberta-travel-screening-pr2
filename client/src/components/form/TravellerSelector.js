import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import { Typography } from '@material-ui/core';
import { Button } from '../generic';
import { StatusCell } from '../generic/StatusCell';

export const TravellerSelector = ({ options, handleSelect, handleDOBAttempts }) => {

  return (
    <Container maxWidth="sm">
      <Box mt={2} mb={4}>
        <h2>Travellers</h2>
        <Grid container spacing={2} xs={12}>
          
          {
            options.map((element, index) => (
              <>
                <Grid item xs={4} sm={6}>
                  <Typography style={{lineHeight: '42px', }}>{`${element.firstName} ${element.lastName}`}</Typography>
                </Grid>
                <Grid item xs={5} sm={4}>
                  <StatusCell submissionStatus={element.submissionStatus} status={element.status} />
                </Grid>
                <Grid item xs={3} sm={2} style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                  <div >
                    <Button
                      data-testid={index} 
                      onClick={() => handleSelect(element)} 
                      style={{display: 'flex'}}
                      text="Select"
                      disabled={element.submissionStatus === 'submitted' || element.status === 'red' || handleDOBAttempts(element.dobVerificationAttempt, element.dobAttemptTime)}
                    />
                  </div>
                </Grid>
              </>
            ))
          }
        </Grid>
      </Box>
    </Container>
  )
}
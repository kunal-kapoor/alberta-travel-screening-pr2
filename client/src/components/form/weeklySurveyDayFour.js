import React from 'react';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { Formik, Form, FastField } from 'formik';
import { useTranslation } from "react-i18next";


import { FocusError, Card, Button } from '../generic';
import { RenderRadioGroup } from '../fields';
import { 
  EmploymentStatus, 
  EmploymentIndustry, 
  Questions, 
  AdultsInHousehold, 
  ChildrenInHousehold, 
  DayFourSurveyValidationSchema, 
  TeamHelpful ,
  DifficultyOptions
} from '../../constants';
import Alert from '@material-ui/lab/Alert';

export const WeeklySurveyDayFour = ({ submit, isFetching}) => {
  const { t } = useTranslation();

  return (
    <Container maxWidth="sm">
      <Alert severity="info">
        Prior to completing today’s daily check-in report, we would like to ask you a few questions 
        about your experience with the program so far. The survey should take you 3 - 5 minutes and 
        this information will be used to improve the program. Once you have completed the survey 
        below, you will be taken to a new page to complete your daily check-in report. We value 
        your time and thank you for sharing your feedback.
      </Alert>
      <Box mt={2} mb={4}>
        <Formik
          validationSchema={DayFourSurveyValidationSchema}
          initialValues={{
            difficultyCompletingRegistration: '',
            difficultyGettingConfirmationId: '',
            difficultyFindingStaff: '',
            easeOfUnderstandingProgram: '',
            difficultyLocatingBooth: '',
            receivedTestResults: '',
            acceptableResponseTime: '', // Conditional A
            acceptableWaitTime: '',     // Conditional B
            difficultyAccessingResult: '',
            difficultyCompletingCheckin: '',
            teamHelpful: '',
            adultsInHousehold: '',
            childrenInHousehold: '',
            employmentStatus: '',
            employmentIndustry: '', // Optional
          }}
          onSubmit={submit}
        >
          {({values, setFieldValue}) => (
            <Form>
              <FocusError/>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Card title={t('Feedback questionnaire')}>
                    <Grid container spacing={2}>
                      <Grid item xs={12}>
                        <Box my={1}>
                          <Box m={1}>
                            <Box my={2}>
                              <FastField
                                name="difficultyCompletingRegistration"
                                label="Did you have difficulties completing the online form to register for this program?"
                                component={RenderRadioGroup}
                                options={DifficultyOptions}
                              />
                            </Box>
                            <Box my={2}>
                              <FastField
                                name="difficultyGettingConfirmationId"
                                label="Once you submitted the online form, did you have difficulties receiving your program Confirmation ID?"
                                component={RenderRadioGroup}
                                options={DifficultyOptions}
                              />
                            </Box>
                            <Box my={2}>
                              <FastField
                                name="easeOfUnderstandingProgram"
                                label="When you arrived to the airport or Coutts border, you were given a program instruction sheet (purple writing). Was the information provided clear?"
                                component={RenderRadioGroup}
                                options={Questions}
                              />
                            </Box>
                            <Box my={2}>
                              <FastField
                                name="difficultyFindingStaff"
                                label="Did you have difficulties finding the registration desk at the airport or Coutts border to complete your enrollment for this program?"
                                component={RenderRadioGroup}
                                options={DifficultyOptions}
                              />
                            </Box>
                            <Box my={2}>
                              <FastField
                                name="acceptableTimeLength"
                                label="Once you found the desk, did you feel the length of time it took to complete your enrollment was acceptable?"
                                component={RenderRadioGroup}
                                options={Questions}
                              />
                            </Box>
                            <Box my={2}>
                              <FastField
                                name="difficultyLocatingBooth"
                                label="Did you have difficulties locating the testing station to receive your COVID-19 test at the airport or Coutts border?"
                                component={RenderRadioGroup}
                                options={DifficultyOptions}
                              />
                            </Box>

                            <Box my={2}>
                              <FastField
                                name="receivedTestResults"
                                label="Have you received the result from the COVID-19 test you took at the airport or Coutts border?"
                                onChange={(event) => {
                                  setFieldValue('receivedTestResults', event.target.value)
                                  setFieldValue('acceptableResponseTime', '')
                                  setFieldValue('acceptableWaitTime', '')
                                }}
                                component={RenderRadioGroup}
                                options={Questions}
                              />
                            </Box>
                            {
                              values.receivedTestResults === 'Yes'
                              ?
                                <>
                                  <Box my={2}>
                                    <FastField
                                      name="acceptableResponseTime"
                                      label="Did you feel the length of time it took to get your test result was acceptable?"
                                      component={RenderRadioGroup}
                                      options={Questions}
                                    />
                                  </Box>
                                  <Box my={2}>
                                    <FastField
                                      name="difficultyAccessingResult"
                                      label="Did you have difficulties accessing your test result through email or text (problems with encryption or other)?"
                                      component={RenderRadioGroup}
                                      options={DifficultyOptions}
                                    />
                                  </Box>
                                </>
                              :
                              values.receivedTestResults === "No"
                              ?
                              <Box my={2}>
                                <FastField
                                  name="acceptableWaitTime"
                                  label="Is the length of time it is taking you to get your test result acceptable?"
                                  component={RenderRadioGroup}
                                  options={Questions}
                                />
                              </Box>
                              : null
                            }

                            <Box my={2}>
                              <FastField
                                name="difficultyCompletingCheckin"
                                label="The program requires you to submit a check-in report each day about symptoms. Have you had difficulties completing these questions?"
                                component={RenderRadioGroup}
                                options={DifficultyOptions}
                              />
                            </Box>
                            <Box my={2}>
                              <FastField
                                name="teamHelpful"
                                label="If you contacted the eHealth team for support, did you find this helpful?"
                                component={RenderRadioGroup}
                                options={TeamHelpful}
                              />
                            </Box>
                            <Box my={2}>
                              <FastField
                                name="adultsInHousehold"
                                label="How many adults live in your household (including you)?"
                                component={RenderRadioGroup}
                                options={AdultsInHousehold}
                              />
                            </Box>

                            <Box my={2}>
                              <FastField
                                name="childrenInHousehold"
                                label="How many children (<18 years) live in your household?"
                                component={RenderRadioGroup}
                                options={ChildrenInHousehold}
                              />
                            </Box>
                            <Box my={2}>
                              <Typography variant="subtitle2">
                                We would like to know how this program may have impacted your employment. 
                              </Typography>
                            </Box>
                            <Box my={2}>
                              <FastField
                                name="employmentStatus"
                                label="What is your current employment status?"
                                component={RenderRadioGroup}
                                onChange={(event) => {
                                  setFieldValue('employmentStatus', event.target.value)
                                  setFieldValue('employmentIndustry', '')
                                }}
                                options={EmploymentStatus}
                              />
                            </Box>
                            {
                              values.employmentStatus === "Employed"
                              ?
                                <Box my={2}>
                                  <FastField
                                    name="employmentIndustry"
                                    label="In which industry do you work?"
                                    component={RenderRadioGroup}
                                    options={EmploymentIndustry}
                                  />
                                </Box>
                                : null
                            }
                          </Box>
                        </Box>
                      </Grid>

                      <Grid item xs={12} sm={12}>
                        <Button
                          text="Submit Survey"
                          disabled={isFetching}
                          type="submit"
                        />
                      </Grid>
                    </Grid>
                  </Card>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </Box>
    </Container>
  )
}
import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import { FastField, Form, Formik } from 'formik';

import { Button } from '../generic';
import { RenderSelectField, RenderDateField } from '../../components/fields';
import { HouseholdMember } from '../../constants';
import moment from 'moment';
import Alert from '@material-ui/lab/Alert';
import { useTranslation } from "react-i18next";

export const HouseholdMemberForm = ({ onSubmit, initialValues, isFetching, disabled }) => {

  const { t, i18n } = useTranslation();

  return (
    <Box pt={[3]} pb={[2]}>
      {disabled && (
      <Container maxWidth="sm">
        <Alert variant="outlined" severity="warning" style={{ color: "black"}}>
          {t("You have exceeded the number verification attempts at entering the correct date of birth. Please try again after 5 minutes.")}
        </Alert>
        </Container>
      )}
      <Container maxWidth="sm">
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Formik
              initialValues={initialValues}
              validationSchema={HouseholdMember}
              onSubmit={onSubmit}
            >
              <Form>
                <Grid container spacing={2}>
                  <Grid item xs={12} md={12}>
                    <FastField
                      name="birthday"
                      label="Date of birth* (YYYY/MM/DD)"
                      placeholder="Required"
                      component={RenderDateField}
                    />
                  </Grid>
                  <Grid item>
                    <Box ml={[1, 0]} mr={[1, 0]}>
                      <Button
                        text="Verify Date of Birth"
                        type="submit"
                        disabled={disabled}
                      />
                    </Box>
                  </Grid>
                </Grid>
              </Form>
            </Formik>
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
}
import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { Formik, Form as FormikForm } from 'formik';
import { useTranslation } from "react-i18next";

import { EnrollmentFormSchema } from '../../../constants';

import { Button, FocusError } from '../../generic';
import { PrimaryContact } from './PrimaryContact';
import { TravelInformation } from './TravelInformation';
import { ArrivalInformation } from './ArrivalInformation';
import { IsolationQuestionnaire } from './IsolationQuestionnaire';
import { Typography } from '@material-ui/core';

export const EnrollmentForm = ({ onSubmit, initialValues, isFetching, isDisabled, canEdit, adminMode, cancelHandler }) => {
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={EnrollmentFormSchema}
      enableReinitialize
      onSubmit={onSubmit}
    >
      {({setFieldValue}) => (
        <FormikForm>
            <FocusError />
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <PrimaryContact setFieldValue={setFieldValue} isDisabled={isDisabled} canEdit={canEdit} />
              </Grid>
              <Grid item xs={12}>
                <ArrivalInformation isDisabled={isDisabled} setFieldValue={setFieldValue} />
              </Grid>
              <Grid item xs={12}>
                <TravelInformation setFieldValue={setFieldValue} isDisabled={isDisabled} />
              </Grid>
              <Grid item xs={12}>
                <IsolationQuestionnaire isDisabled={isDisabled} adminMode={adminMode}/>
              </Grid>
              {(!isDisabled) && (
                adminMode
                  ?
                  <Grid container xs={12} sm={12} justify="space-between" style={{ margin: '16px 8px 16px 8px'}}>
                    <Grid item xs={12} sm={5}>
                      <Button
                        text="Cancel"
                        variant="outlined"
                        onClick={cancelHandler}
                        loading={isFetching}
                      />
                    </Grid>
                    <Grid item xs={12} sm={5}>
                      <Button
                        text="Submit"
                        type="submit"
                        variant="contained"
                        loading={isFetching}
                      />
                    </Grid>
                  </Grid>
                  :
                  <Grid item xs={12} sm={7} md={6} lg={4} xl={2}>
                    <Box ml={[1, 0]} mr={[1, 0]}>
                      <Button
                        text="Continue"
                        type="submit"
                        loading={isFetching}
                      />
                    </Box>
                  </Grid>  
              )}
            </Grid>
          </FormikForm>
        )}
      </Formik>
    );
};
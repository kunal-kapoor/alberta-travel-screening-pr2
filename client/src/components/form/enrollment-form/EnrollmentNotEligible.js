import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

import IsolationPlanFailIcon from '../../../assets/images/isolation-fail-traveller.svg';

export const EnrollmentNotEligible = () => (
    <Container maxWidth="sm">
        <Box mt={4} mb={4}>
            <Box p={2} border={1} borderRadius={4} borderColor="divider">
                <Grid container spacing={4} align="center">
                <Grid item xs={12} sm={6} alignItems="center" container>
                    <div>
                    <Box mb={2} component={Typography} variant="subtitle2" color="text.secondary">
                        {"You are not eligible to participate in the Alberta COVID-19 Border Testing Pilot Program."}
                    </Box>
                    <Typography variant="body2" color="textPrimary">
                        {"Please proceed with your quarantine plan."}
                    </Typography>
                    </div>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Box p={[2.5, 4]} border={1} borderRadius={4} borderColor="divider">
                    <img
                        src={IsolationPlanFailIcon}
                        height={96}
                    />
                    </Box>
                </Grid>
                </Grid>
            </Box>
        </Box>
    </Container>
);
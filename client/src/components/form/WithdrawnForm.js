import React from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FastField, Formik, Form } from 'formik';
import { useTranslation } from "react-i18next";

import { Button } from '../generic';
import { RenderSelectField } from '../fields';


import { FocusError } from '../generic';
import { WithdrawnReason } from '../../constants';
import { useModal } from '../../hooks';

export const WithdrawnForm = ({ isHousehold, onSubmit }) => {
    const { closeModal } = useModal();

    return (
        <Formik
            initialValues={{
                'withdrawnReason': WithdrawnReason[0].value
            }}
            onSubmit={onSubmit}
        >
            <Form>
                <FocusError />
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Box my={1}>
                            <Typography variant="body">{`Are you sure you want to withdraw a ${isHousehold? 'household':'traveller'}: `}</Typography>
                            <Box mt={1}>
                                <FastField
                                    name='withdrawnReason'
                                    label='Reason for withdrawal'
                                    component={RenderSelectField}
                                    options={WithdrawnReason}
                                />
                            </Box>
                        </Box>
                    </Grid>
                </Grid>
                <Box mt={2}>
                    <Grid container spacing={2} justify='center'>
                        <Grid item xs={6} sm={6} >
                            <Button
                                style={{ minWidth: 200 }}
                                color="primary"
                                variant="outlined"
                                size="small"
                                fullWidth={false}
                                text="Cancel"
                                onClick={() => closeModal()}
                            />
                        </Grid>
                        <Grid item xs={6} sm={6}>
                            <Button
                                style={{ minWidth: 200 }}
                                color="primary"
                                size="small"
                                fullWidth={false}
                                text="Confirm"
                                type="submit"
                            />
                        </Grid>
                    </Grid>
                </Box>
            </Form>
        </Formik >

    );
};

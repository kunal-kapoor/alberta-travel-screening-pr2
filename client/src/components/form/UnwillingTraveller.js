import React from 'react';
import Box from '@material-ui/core/Box'
import { Button, StyledDialogTitle, StyledDialogContent, StyledDialogActions } from '../generic';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle'
import Typography from '@material-ui/core/Typography'
import {
    AxiosPrivate,
} from '../../utils'
import { useToast } from '../../hooks';
import { Formik, Form as FormikForm, FastField } from 'formik';
import {BackOfficeUnwillingTravellerSchema} from '../../constants/validation'
import { RenderTextField, RenderSelectField }  from '../../components/fields'
export const UnwillingTravellerButton = () => {
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
      };
    
      const handleClose = () => {
        setOpen(false);
      };
    return (
        <div style = {{display:"inline"}}>
            <Box
                component={Button}
                mr={3}
                style={{minWidth: 175}}
                text="Report Unwilling Traveller"
                onClick={handleClickOpen}
                variant="outlined"
                fullWidth={false}
                size="small"/>
            <UnwillingTravellerDialog open={open} onClose={handleClose} />
        </div>
    )
}

export const UnwillingTravellerDialog = (props)=> {
    const { onClose, open } = props;
    const [isFetching, setFetching] = React.useState(false);
    const { openToast } = useToast();

    const handleClose = () => {
      onClose();
    };
  
    const submitReport = async (values) => {
        try{
            setFetching(true)

            await AxiosPrivate.post(`/api/v1/admin/back-office/unwilling-traveller`, {
                numTravellers: parseInt(values.counter),
                note:values.notes
            });
            setFetching(false)
            openToast({status:"success",message:"Report submitted"})
            onClose();
        } catch(e) {
            openToast({status:"error",message:e.message})
            setFetching(false)
        }
    };
    
    if(isFetching){
        return ( 
            <Dialog 
                fullWidth="true"
                onClose={handleClose}
                aria-labelledby="simple-dialog-title"
                open={open}>
                <DialogTitle id="simple-dialog-title">Submitting report...</DialogTitle>
           </Dialog>
        )
    }

    return (
    <Dialog 
        fullWidth="true"
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
    >
        <Formik
            validationSchema ={BackOfficeUnwillingTravellerSchema}
            onSubmit={values => submitReport(values)}
            initialValues={{
                'counter':1,
                'notes':''
            }}
        >
            <FormikForm>
                <StyledDialogTitle component="div">
                    <Typography component="div" variant="h4">Report Unwilling Traveller</Typography>
                </StyledDialogTitle>
                <StyledDialogContent>
                    <FastField
                        component= {RenderSelectField}
                        name="counter"
                        type="number"
                        label="Number of Travellers *"
                        variant="outlined"
                        options={[...Array(10)].map((_, index) =>
                            ({ value: index + 1, label: index + 1 })
                        )}
                        style={{
                            margin:"0px 30px",
                            width:"90%"
                        }}
                    />
                    <FastField
                        component={RenderTextField}
                        name="notes"
                        placeholder="Please provide any relevant details."
                        variant="outlined"
                        label="Notes"
                        style={{
                            margin:"0px 30px",
                            width:"90%"
                        }}
                        multiline
                        rows={5}
                    />
                </StyledDialogContent>

                <StyledDialogActions>
                    <Button
                        style={{ minWidth: 150 }}
                        text="Close"
                        onClick={handleClose}
                        color="primary"
                        variant="outlined"
                        size="small"
                        fullWidth={false}
                    />
                    <Button
                        style={{ minWidth: 150 }}
                        text="Submit"
                        type="submit"
                        color="primary"
                        size="small"
                        disabled={isFetching}
                        fullWidth={false}
                    />
                </StyledDialogActions>
            </FormikForm>
        </Formik>
    </Dialog>
    );
  }
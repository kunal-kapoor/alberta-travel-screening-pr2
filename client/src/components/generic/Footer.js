import React from 'react';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import { useTranslation } from "react-i18next";
import GooglePlayStore from '../../assets/images/google-play-badge.png';
import AppStore from '../../assets/images/app-store-badge.png';

export const Footer = () => {
  const { t } = useTranslation();

  return (
    <Box mt="auto">
      <Container maxWidth="sm">
        <Box borderTop={1} borderColor="divider" bgcolor="common.lightGrey" mt={2} mb={4}>
          <Container maxWidth="md">
            <Box pt={2} pb={1}>
              <Grid container spacing={1} justify="center" alignItems="center">
                <Grid item>
                  <Grid container spacing={1}>
                    <Grid item xs={12}>
                      <Typography variant="h3" align="center">ABTraceTogether</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="body2" align="center">Help prevent the spread of COVID-19 with the ABTraceTogether mobile app. <Link href="https://www.alberta.ca/ab-trace-together.aspx" target="_blank">Learn More</Link></Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Grid container spacing={1} alignItems="center" justify="space-between" mt={2}>
                    <Grid item>
                      <Link href="https://play.google.com/store/apps/details?id=ca.albertahealthservices.contacttracing" target="_blank">
                        <img
                          src={GooglePlayStore}
                          alt="Google Play Store Link"
                          height={50}
                        />
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link href="https://apps.apple.com/ca/app/abtracetogether/id1508213665" target="_blank">
                        <img
                          src={AppStore}
                          alt="App Store Link"
                          height={50}
                        />
                      </Link>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          </Container>
        </Box>
      </Container>
      <Box borderTop={1} borderColor="divider" bgcolor="common.lightGrey">
        <Container maxWidth="md">
          <Box pt={4} pb={4}>
            <Grid container alignItems="center" justify="space-between">
              <Grid item>
                <Grid container spacing={3}>
                  <Grid item xs={12} sm={true}>
                    <Link href="https://www.alberta.ca/disclaimer.aspx" target="_blank">{t("Disclaimer")}</Link>
                  </Grid>
                  <Grid item xs={12} sm={true}>
                    <Link href="https://www.alberta.ca/privacystatement.aspx" target="_blank">{t("Privacy")}</Link>
                  </Grid>
                  <Grid item xs={12} sm={true}>
                    <Link href="https://www.alberta.ca/accessibility.aspx" target="_blank">{t("Accessibility")}</Link>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Box mt={[3, 0]}>
                  <Typography variant="body2" color="textSecondary">
                    &copy; 2020 {t("Government of Alberta")}
                  </Typography>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </Box>
      <Box pt={1} pb={1} bgcolor="secondary.main" />
    </Box>
  );
};
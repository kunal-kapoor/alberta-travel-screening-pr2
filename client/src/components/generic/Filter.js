import React from 'react';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { Button } from '.';

const useStyles = makeStyles((theme) => ({
  button: {
    borderRadius: '16px',
    boxShadow: 'none',
    background: theme.palette.common.white,
    color: theme.palette.common.darkGrey,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  buttonSelected: {
    background: `${theme.palette.primary.main} !important`,
    color: `${theme.palette.common.white} !important`,
  },
}));

export const Filter = ({ value, options, onClick, alwaysSelected=false, endIcon=undefined }) => {
  const classes = useStyles();
  return (options.map(({ value: v, label }) => (
    <Box key={v} mx={0.75} display="inline-block">
      <Button
        endIcon={endIcon}
        className={clsx(classes.button, { [classes.buttonSelected]: alwaysSelected || v === value })}
        text={label}
        onClick={() => onClick(v)}
        fullWidth={false}
        size="small"
      />
    </Box>
  )));
};

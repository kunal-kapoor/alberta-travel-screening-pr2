import React from 'react';

export const EligibilityQuestions = [
    {
        name: "residency",
        required: "Must specify if permitted entry into Canada",
        question: "Are all members of your travelling party a returning Canadian citizen, a permanent resident or a foreign national currently permitted entry into Canada (for example, immediate and extended family members of Canadian citizens and permanent residents)?",
        eligibleAnswer: "Yes"
    },
    {
        name: "symptoms",
        required: "Must specify if anyone in your travelling party have COVID-19 symptoms",
        question: <span>Does anyone in your travelling party currently have any signs or <a href="https://www.alberta.ca/covid-19-testing-in-alberta.aspx" target="_blank">symptoms</a> of COVID-19?</span>,
        eligibleAnswer: "No"
    },
    {
        name: "covidContact",
        required: "Must specify if anyone in your travelling party have been in contact with confirmed COVID-19 case",
        question: "Have you, or anyone in your travelling party, been in contact with a confirmed COVID-19 case in the last 14 days?",
        eligibleAnswer: "No"
    },
    {
        name: "quarantinePlan",
        required: "Must specify if your travelling party has an acceptable quarantine plan",
        question: "Does everyone in your travelling party have an acceptable quarantine plan?",
        eligibleAnswer: "Yes"
    },
    {
        name: "remainingInAlberta",
        required: "Must specify if all members are remaining in Alberta for 14 days",
        question: "Will all members of your travelling party remain in Alberta for 14 days?  If not, are you departing the country?",
        eligibleAnswer: "Yes"
    },
    {
        name: "useABTraceTogetherApp",
        required: "Must specify if planning to use ABTraceTogether App",
        question: "Downloading the ABTraceTogether (ABTT) app is a requirement for your participation in the border pilot program. Please confirm you commit to downloading and enrolling in ABTT upon arrival in Canada as well as ensuring ABTT is operating on your phone if and when you are permitted to leave quarantine and when you are interacting with individuals outside your household. It is recommended that you leave ABTT installed for 14 days after exiting the border pilot to support contact tracing.",
        eligibleAnswers: ["Yes", "No"]
    }
];

// Returns true if the submitted answers
// all match the eligibleAnswer of the corresponding 
// EligibilityQuestion
export const answersAreEligible = (answers) => {
    return !Object.entries(answers).find(([question, answer]) => {
        const q = EligibilityQuestions.find(q => q.name === question);

        if(q.eligibleAnswers) {
            return !q.eligibleAnswers.includes(answer);
        } else {
            return q.eligibleAnswer !== answer;
        } 
    });
}
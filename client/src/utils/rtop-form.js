import { Place, Vehicle, Places, Vehicles, Question, Province, Provinces, AirlinesAndAbbreviationValues, AirlinesAndAbbreviations } from '../constants';
import { dateToString } from './date';

export const serializeEnrollmentFormValues = (values) => {
  const valuesCopy = { ...values };

  if (valuesCopy.hasAdditionalTravellers === Question.No) {
    valuesCopy.additionalTravellers = [];
  }
  if (valuesCopy.typeOfPlace === Place.Other) {
    valuesCopy.typeOfPlace = valuesCopy.typeOfPlaceDetails;
  }
  if (valuesCopy.howToGetToPlace === Vehicle.Other) {
    valuesCopy.howToGetToPlace = valuesCopy.howToGetToPlaceDetails;
  }
  if (valuesCopy.provinceTerritory === Province.Other) {
    valuesCopy.provinceTerritory = valuesCopy.provinceTerritoryDetails;
  }
  if (valuesCopy.airline === AirlinesAndAbbreviationValues.Other) {
    valuesCopy.airline = valuesCopy.airlineDetails;
  }

  valuesCopy.preDepartureTest = (valuesCopy.noPreDepartureTest || !valuesCopy.preDepartureTestDate)? 'No': 'Yes';

  delete valuesCopy.typeOfPlaceDetails;
  delete valuesCopy.howToGetToPlaceDetails;
  delete valuesCopy.provinceTerritoryDetails;
  delete valuesCopy.airlineDetails;
  delete valuesCopy.noPreDepartureTest;

  delete valuesCopy.numberOfAdditionalTravellers;

  return valuesCopy;
};

export const normalizeEnrollmentFormValues = (values) => {
  const valuesCopy = { ...values };

    const fillOtherDetails = (optionsList, dropDownValue, detailsValue, criteria) => {
      if (!optionsList.find(eachValue => eachValue.value === valuesCopy[dropDownValue])) {
        valuesCopy[detailsValue] = valuesCopy[dropDownValue];
        valuesCopy[dropDownValue] = criteria;
      } else {
        valuesCopy[detailsValue] = '';
      }  
    }

    fillOtherDetails(Places, 'typeOfPlace', 'typeOfPlaceDetails', Place.Other);
    fillOtherDetails(Vehicles, 'howToGetToPlace', 'howToGetToPlaceDetails', Vehicle.Other);
    fillOtherDetails(Provinces, 'provinceTerritory', 'provinceTerritoryDetails', Province.Other);
    fillOtherDetails(AirlinesAndAbbreviations, 'airline', 'airlineDetails', AirlinesAndAbbreviationValues.Other);

  if (valuesCopy.additionalTravellers?.length > 0) {
    valuesCopy.additionalTravellers.forEach(traveller => traveller.dateOfBirth = dateToString(traveller.dateOfBirth, false));
  }

  valuesCopy.dateOfBirth = dateToString(valuesCopy.dateOfBirth, false);
  valuesCopy.additionalCitiesAndCountries = valuesCopy.additionalCitiesAndCountries || [];
  valuesCopy.numberOfAdditionalTravellers = valuesCopy.additionalTravellers?.length || 0;
  valuesCopy.numberOfAdditionalCitiesAndCountries = valuesCopy.additionalCitiesAndCountries?.length || 0;
  valuesCopy.noPreDepartureTest = values.preDepartureTest === 'No';

  return valuesCopy;
}

export const isAirport = nameOfAirportOrBorderCrossing => {
  return nameOfAirportOrBorderCrossing?.includes('Airport');
}
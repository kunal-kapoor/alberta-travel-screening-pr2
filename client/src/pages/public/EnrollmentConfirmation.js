import React, { useEffect } from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import { Prompt, Redirect, useLocation } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { Route } from '../../constants';
import { useConfirmationOnExit } from '../../hooks';

import { Page } from '../../components/generic';
import { ConfirmationMessage } from '../../components/form/enrollment-form/ConfirmationMessage';

export default () => {
  const { state } = useLocation();
  const { travellers, viableIsolationPlan } = state || {};
  const { t } = useTranslation();

  if (!state) return <Redirect to={Route.Root} />;

  useEffect(() => {
    document.tile = 'Alberta COVID-19 Border Testing Pilot Program'
  }, [])

  useConfirmationOnExit();

  return (
    <Page>

      {/** Prompts the user before navigating backwards or forwards */}
      <Prompt message={t("Are you sure you want to leave?")} />

      {/** Orange Banner */}
      <Box pt={[1.5, 3]} pb={[1.5, 3]} bgcolor="warning.main">
        <Container maxWidth="md">
          <Box display="flex" alignItems="center">
            <InfoOutlinedIcon />
            <Box ml={1.5}>
              <Typography variant="subtitle2" color="textPrimary">
                {t("Do not close this page.") + " " + t("You may be asked to present your confirmation below.")}
              </Typography>
            </Box>
          </Box>
        </Container>
      </Box>

      {/** Blue Banner */}
      <Box pt={[3, 6]} pb={[3, 6]} bgcolor="secondary.main" color="common.white">
        <Container maxWidth="md">
          <Box mb={2.5} component={Typography} variant="h1">
            {t("Thank You.")}
          </Box>
          <Typography variant="subtitle1">
            {t("Your form has been submitted.")}
          </Typography>
        </Container>
      </Box>

      {/** Message */}
      <ConfirmationMessage travellers={travellers} viableIsolationPlan={viableIsolationPlan} />

    </Page>
  );
};

import React from 'react';
import Grid from '@material-ui/core/Grid';
import { Formik, Form as FormikForm } from 'formik';
import { useTranslation } from "react-i18next";

import { EnrollmentFormSchema, EligibilityCheckValidationSchema } from '../../constants';
import { FastField } from 'formik';

import { Card, Button, InputFieldLabel } from '../../components/generic';
import { FocusError } from '../../components/generic';
import { Questions } from '../../constants';
import { RenderRadioGroup } from '../../components/fields';
import { Box, Typography } from '@material-ui/core';
import { EligibilityQuestions } from '../../constants/eligibilityQuestions';


export const EligibilityCheckQuestionnaire = ({ onSubmit, initialValues, isDisabled, isFetching, adminMode, cancelHandler }) => {
  const { t } = useTranslation();

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={EligibilityCheckValidationSchema}
      onSubmit={onSubmit}
    >
      <FormikForm>
        <FocusError />
        <Card title={t('Pilot Eligibility Check')}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Typography variant="subtitle2">Note: Please answer the following questions for yourself and everyone in your travelling party who will be quarantining together</Typography>
              </Grid>
                {EligibilityQuestions.map(({question, name}) => (
                    <Grid item xs={12} key={name}>
                        <InputFieldLabel label={question}></InputFieldLabel>
                        <FastField
                            name={name}
                            label=""
                            component={RenderRadioGroup}
                            disabled={isDisabled}
                            options={Questions}
                        />
                    </Grid>
                ))}

                {(!isDisabled) && (
                  adminMode
                    ?
                    <Grid container xs={12} sm={12} justify="space-between" style={{ margin: '16px 8px 16px 8px'}}>
                      <Grid item xs={12} sm={5}>
                        <Button
                          text="Cancel"
                          variant="outlined"
                          onClick={cancelHandler}
                          loading={isFetching}
                        />
                        
                      </Grid>
                      <Grid item xs={12} sm={5}>
                        <Button
                          text="Submit"
                          type="submit"
                          variant="contained"
                          loading={isFetching}
                        />
                      </Grid>
                    </Grid>
                    :
                    <Grid item xs={12} sm={7} md={6} lg={4} xl={2}>
                      <Box ml={[1, 0]} mr={[1, 0]}>
                        <Button
                          text="Continue"
                          type="submit"
                          loading={isFetching}
                        />
                      </Box>
                    </Grid>
                )}
            </Grid>            
        </Card>
      </FormikForm>
    </Formik>
  );
};
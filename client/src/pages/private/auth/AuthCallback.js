import React, { useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';


import { Page } from '../../../components/generic';

/**
 * Page used as a callback for when authentication with AppID is successful.
 */
export default () => {

  useEffect(() => {
      // Post message to the opener of this window if present.
      window.opener && window.opener.postMessage('auth_successful');

      // Close the current window
      window.opener && window.close();
  },[]);

  // Provide a fallback in case closing of the window above fails
  return (
    <Page hideFooter centerContent>
      <Grid container justify="center">
        <Grid item xs={12} sm={8} md={6} lg={4} xl={3} align="center">
            <Typography variant="body1" color="textSecondary" noWrap gutterBottom>Authentication successul. Close window and carry on.</Typography>
        </Grid>
      </Grid>
    </Page>
  );
};

import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { Formik, Form as FormikForm, FastField } from 'formik';

import { useMonitoringReportExport } from '../../../hooks';

import { Page, Button } from '../../../components/generic';
import { RenderSelectField, RenderDateField } from '../../../components/fields';
import moment from "moment";
import { RTOPExportReportValidationSchema } from '../../../constants/validation';
import { EnrollmentStatusReportColumns } from '../../../constants';

export default () => {
    const {
        isFetching,
        executeReport,
    } = useMonitoringReportExport();

    return (
        <Box pt={[3, 3]} pb={[3, 3]}>
            <Container maxWidth="md">
                <Typography variant="h3">PHAC Extract</Typography>
                <Grid container spacing={2} style={{marginTop: '3px'}}>
                    <Grid item xs={12} sm={12} md={12}>
                        <Formik
                            validationSchema={RTOPExportReportValidationSchema}
                            onSubmit={values => executeReport(values)}
                            initialValues={{
                                'date': '',
                                'type': 'enrolled'
                            }}
                        >
                            <FormikForm>
                                <Grid container justify="space-between" alignItems="center" spacing={2}>
                                    <Grid item xs={12} sm={6} md={4}>
                                        <FastField
                                            name="date"
                                            label="Date* (YYYY/MM/DD)"
                                            component={RenderDateField}
                                            placeholder="Required"
                                            disableFuture={true}
                                            maxDate={moment().local(true).subtract('1', 'day')}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6} md={4}>
                                        <FastField
                                            name="type"
                                            label="Enrollment Status*"
                                            component={RenderSelectField}
                                            placeholder="Required"
                                            options={EnrollmentStatusReportColumns}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6} md={4}>
                                        <h6></h6>
                                        <Button
                                            disabled={isFetching}
                                            type="submit"
                                            text="Export CSV"
                                        />
                                    </Grid>
                                </Grid>
                            </FormikForm>
                        </Formik>
                    </Grid>
                </Grid>
            </Container>
        </Box>
    );
};

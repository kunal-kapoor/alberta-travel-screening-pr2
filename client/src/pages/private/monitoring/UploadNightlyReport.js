import React, { useEffect, useState } from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import * as csv from 'csvtojson';

import { Page, Button, StyledTableCell, StyledTableRow } from '../../../components/generic';
import { RenderFileDropzone } from '../../../components/fields/RenderFileDropzone';
import { useToast } from '../../../hooks';
import { AxiosPrivate } from '../../../utils';
import Upload from '../../../assets/images/upload.png';
import moment from 'moment-timezone';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import MuiTable from '@material-ui/core/Table';

export default () => {
  const { openToast } = useToast();
  const [fileData, setFileData] = useState();
  const [isFileUploaded, setFileUploaded] = useState(false);
  const [isFileValid, setFileValidity] = useState(undefined);
  const [errorData, setErrorData] = useState(undefined);

  const clear = () => {
    setFileData(undefined);
    setFileUploaded(false);
    setFileValidity(undefined);
    setErrorData(undefined);
  };

  const handleFileDrop = (file) => {
    clear();

    if (file.length) {
      const reader = new FileReader();
      reader.onload = function(e) {
        const contents = e.target.result;
        csv().fromString(contents).then((json) => {
          // Remove first index - headers
          // Parse confirmation number to correct format
          let request = json
            .map(res => {
              const length = res.TRAVELER_ID?.length;
              const parsedConfirmationNumber = res.TRAVELER_ID.slice(1, length -1) + '-' + res.TRAVELER_ID.slice(length - 1)
              return (
                {
                  confirmationNumber: parsedConfirmationNumber,
                  testedAt: res['2ND_SPEC_COLL_DTM'] && moment(res['2ND_SPEC_COLL_DTM'])
                }
              )
            })
            .filter(rec => !!rec.testedAt);

          let errors = request
            .filter(r => !r.testedAt.isValid() || !r.confirmationNumber)
            .map(r => ({confirmationNumber: r.confirmationNumber || 'NOT INCLUDED', failureReason: !r.confirmationNumber? 'missing_confirmation_number': 'invalid_date'}));

          if(errors.length) {
            setErrorData(errors);
          } else if (request.length) {
            setFileData(request);
            setFileUploaded(true);
            setFileValidity(true)
          } else setFileValidity(false)

        });
      };
      
      reader.readAsText(file[0]);
    }
  }

  const handleUploadRequest = async () => {
    if (fileData) {
      setErrorData(undefined);

      try {
          const uploadResponse = await AxiosPrivate.post('/api/v2/rtop-admin/test-results', {results: fileData});
          if(uploadResponse.errors?.length) {
            setErrorData(uploadResponse.errors);
            openToast({ status: 'error', message: 'Failed to upload testing records'});
          } else {
            clear();
            openToast({ status: 'success', message: 'Successfully submitted testing records'});
          }
      } catch(e) {
        openToast({ status: 'error', message: e.message || "Failed to upload testing recrods."});
      }
    }
  }

  const failureReason = reason => {
    return {
      'invalid_confirmation_number': 'Confirmation Number does not match a traveller in the system',
      'missing_confirmation_number': 'Confirmation Number for record is missing',
      'invalid_date': 'The given 2ND_SPEC_COLL_DTM date is not in a valid format.',
    }[reason] || reason;
  }

  useEffect(() => {
    document.title = 'Alberta COVID-19 Border Testing Pilot Program - Testing Report Upload'
  }, [])

  return (
    <Page hideFooter>
      {/** Blue Banner */}
      <Box pt={[3, 4, 6]} pb={[3, 4, 6]} bgcolor="secondary.main" color="common.white">
        <Container maxWidth="md">
          <Typography variant="h1">Upload Testing Report</Typography>
        </Container>
      </Box>

      {/** White Banner */}
      <Box
        mt={4} mb={4}
      >
        <Container maxWidth="md" m>

          {errorData && (
          <>
          <h2>The following records failed to upload</h2>
          <MuiTable style={{ tableLayout: 'fixed' }} size="small">
            <TableHead>
              <StyledTableRow>
                <StyledTableCell align="left">Confirmation Number</StyledTableCell>
                <StyledTableCell align="left">Failure Reason</StyledTableCell>
              </StyledTableRow>
            </TableHead>
            <TableBody>
              {errorData.map(row => (
                <StyledTableRow key={row.confirmationNumber}>
                  <StyledTableCell scope="row">
                    {row.confirmationNumber}
                  </StyledTableCell>
                  <StyledTableCell scope="row">
                    {failureReason(row.failureReason)}
                  </StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </MuiTable>
          </>)}


          <Grid container justify="space-between" alignItems="center" spacing={2}>
            <Grid item xs={12} sm={12}>
              <RenderFileDropzone 
                fileUploaded={isFileUploaded}
                displayValidity={isFileValid}
                uploadCallbackHandler={(file) => handleFileDrop(file)} 
                icon={Upload} 
              />
            </Grid>
            <Grid item xs={12} sm={3  }>
              <Button 
                text="Submit Report"
                onClick={handleUploadRequest}
                disabled={!isFileUploaded}
              />
            </Grid>
          </Grid>
        </Container>
      </Box>
    </Page>
  )
}
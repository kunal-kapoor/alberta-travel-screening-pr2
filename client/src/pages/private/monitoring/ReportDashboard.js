import React from 'react';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

import { Page, SimpleTabs } from '../../../components/generic';
import ExportReportDashboard from './ExportReport';
import ReportDashboard from './ReportStats';


export default () => {
    return (
        <Page hideFooter>
            {/** Blue Banner */}
            <Box pt={[3, 4, 6]} pb={[3, 4, 6]} bgcolor="secondary.main" color="common.white">
                <Container maxWidth="md">
                    <Typography variant="h1">Report Dashboard</Typography>
                </Container>
            </Box>
            <Box pt={[3, 3]} pb={[3, 3]}>
                <Container maxWidth="md">
                    <SimpleTabs
                        children={{
                            "Report Stats": <ReportDashboard />,
                            "Export Reports": <ExportReportDashboard />
                        }}
                    />
                </Container>
            </Box>
        </Page>
    );
};

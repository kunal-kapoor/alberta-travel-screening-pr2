import React from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import { Formik, Form as FormikForm, FastField } from 'formik';

import { useMonitoringReportLookup } from '../../../hooks';

import { Table, Button } from '../../../components/generic';
import { RenderSelectField, RenderDateField } from '../../../components/fields';
import { EnrollmentStatusReportColumns } from '../../../constants';
import moment from "moment";
import { RTOPReportStatsValidationSchema } from '../../../constants/validation';

export default () => {
  const {
    tableData,
    isFetching,
    hasExecuted,
    executeReport,
  } = useMonitoringReportLookup();

  return (
    <>
      <Box pt={[3, 3]} pb={[3, 3]}>
        <Container maxWidth="md">
          <Grid container justify={"space-between"} alignItems="center" spacing={2}>
            <Grid item xs={12} sm={12} md={12}>
              <Formik
                validationSchema={RTOPReportStatsValidationSchema}
                onSubmit={values => executeReport(values)}
                initialValues={{
                  'date': '',
                  'type': 'enrolled'
                }}
              >
                <FormikForm>
                  <Grid container justify="space-between" alignItems="center" spacing={1}>
                    <Grid item xs={12} sm={6} md={4}>
                      <FastField
                        name="date"
                        label="Date* (YYYY/MM/DD)"
                        component={RenderDateField}
                        placeholder="Required"
                        disableFuture={true}
                        maxDate={moment().local(true).subtract('1', 'day')}
                      />
                    </Grid>
                    <Grid item xs={12} sm={6} md={4}>
                      <FastField
                        name="type"
                        label="Enrollment Status*"
                        component={RenderSelectField}
                        placeholder="Required"
                        disabled={true}
                        options={EnrollmentStatusReportColumns}
                      />
                    </Grid>
                    <Grid item xs={12} sm={6} md={4}>
                      <h6></h6>
                      <Button
                        disabled={isFetching}
                        type="submit"
                        text="Fetch Stats"
                      />
                    </Grid>
                  </Grid>
                </FormikForm>
              </Formik>
            </Grid>
          </Grid>
        </Container>
      </Box>

      {/** Table */}
      <Box py={3}>
        <Container maxWidth="md">
          {hasExecuted && <Table
            columns={tableData.columns}
            rows={tableData.rows}
            isLoading={isFetching}
          />}
        </Container>
      </Box>
    </>
  );
};

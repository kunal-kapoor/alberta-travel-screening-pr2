import { useState } from 'react';

import { AxiosPrivate, AxiosPublic, serializeEnrollmentFormValues } from '../utils';
import { useToast } from '.';
import { useTranslation } from "react-i18next";

export const useAgentEnrollmentForm = () => {
  const [isFetching, setFetching] = useState(false);
  const [response, setResponse] = useState();
  const { openToast } = useToast();
  const { t } = useTranslation();

  return {
    isFetching,
    response,
    submit: async (values) => {
      try {
        setFetching(true);
        const {token} = await AxiosPublic.post('/api/v2/rtop/traveller/verification', { email: '', phoneNumber: '', contactMethod: 'callIn' });
        setResponse(await AxiosPublic.post(`/api/v2/rtop/traveller/enroll/${token}`, serializeEnrollmentFormValues(values)));
      } catch (e) {
        openToast({ status: 'error', message: e.message || t("Failed to submit form") });
      } finally {
        setFetching(false);
      }
    }
  }
};

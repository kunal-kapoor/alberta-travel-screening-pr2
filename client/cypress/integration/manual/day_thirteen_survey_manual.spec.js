import { fillThirteenDaySurvey, fillThirteenDayTravel } from "../utils/utils";

describe("Thirteen day survey manual test", () => {
    const token = prompt("Enter a token for a thirteen-day household", "")
    beforeEach(() => {
        cy.visit(`http://localhost:3000/daily/${token}`);
    })


    it("Fails submission validation", () => {
        fillThirteenDaySurvey({ omitOne: true });
        cy.contains("Submit Survey").click();
        cy.contains("This question is required")
    })

    it("Resets travel fields on change", () => {
        fillThirteenDaySurvey({ omitOne: false });

        cy.get('[name="potentialTravel"]').check('Yes');
        cy.get('[name="potentialLostIncome"]').check('Yes');

        fillThirteenDayTravel();

        cy.get('[name="potentialTravel"]').check('No');
        cy.get('[name="potentialTravel"]').check('Yes');
        cy.contains("Submit Survey").click();
        cy.contains("This question is required");

        cy.get('[name="potentialLostIncome"]').check('Yes');
        cy.contains("Submit Survey").click();
        cy.contains("Select atleast one option");

        cy.get('[name="potentialLostIncomeDays.vacation"]').check();
        cy.contains("Submit Survey").click();
        cy.contains("Number of days must be selected and less than or equal to 14")
    })

    it("Resets cost field on change", () => {
        fillThirteenDaySurvey({ omitOne: false });

        cy.get('[name="wouldCoverCost"]').check('Yes');
        cy.get('[id="mui-component-select-costPerPerson"').click();
        cy.get('[data-value="$150"]').click();

        cy.get('[name="wouldCoverCost"]').check('No');
        cy.get('[name="wouldCoverCost"]').check('Yes');

        cy.contains("Submit Survey").click();
        cy.contains("This question is required")
    })

    it("Successfully submits the thirteen day survey", () => {
        fillThirteenDaySurvey({ omitOne: false });

        cy.contains("Submit Survey").click();
        cy.contains("Survey completed successfully!")
    })

})
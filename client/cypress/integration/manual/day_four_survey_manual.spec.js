import { fillFourDaySurvey } from "../utils/utils";

describe("Three day survey manual test", () => {
    const token = prompt("Enter a token for a third-day household", "")
    beforeEach(() => {
        cy.visit(`http://localhost:3000/daily/${token}`);
    })


    it("Fails submission validation", () => {
        fillFourDaySurvey({ omitOne: true });
        cy.contains("Submit Survey").click();
        cy.contains("This question is required")
    })

    it("Resets wait time field on change", () => {
        fillFourDaySurvey({ omitOne: false });

        cy.get('[name="receivedTestResults"]').check('Yes');
        cy.get('[name="receivedTestResults"]').check('No');

        cy.contains("Submit Survey").click();
        cy.contains("This question is required")
    })

    it("Resets employment industry field on change", () => {
        fillFourDaySurvey({ omitOne: false });

        cy.get('[name="employmentStatus"]').check('Unemployed');
        cy.get('[name="employmentStatus"]').check('Employed');
        cy.contains("Submit Survey").click();
        cy.contains("This question is required")
    })

    it("Successfully submits the four day survey", () => {
        fillFourDaySurvey({ omitOne: false });

        cy.contains("Submit Survey").click();
        cy.contains("Survey completed successfully!")
    })

})
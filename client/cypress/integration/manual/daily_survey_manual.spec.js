import { completeDailyForHousehold } from "../utils/utils";

describe("Daily Survey flow manual test - tester input", () => {

  it("Uses tester input to test fill the daily survey", () => {
    const surveyToken = prompt("Enter the daily survey token", "");
    const numberOfSurveys = prompt("Enter the number of surveys to attempt to fill (number of household members)", "");
    cy.visit(`http://localhost:3000/daily/${surveyToken}`);

    completeDailyForHousehold({numberOfSurveys: numberOfSurveys, fillBirthdate: true});

    cy.contains("Thank you for submitting your symptoms.");
  })
})
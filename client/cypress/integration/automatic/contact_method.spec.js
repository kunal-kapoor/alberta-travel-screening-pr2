import { passEligibilityStep } from '../utils/utils';


describe("Contact Form", () => {
  beforeEach(() => {
    // reset and pass eligibility validation
    cy.visit("http://localhost:3000/enroll");
    passEligibilityStep();
  })

  describe("Fails contact validation", () => {

    it("Fails Email validation with incorrect pattern", () => {
      cy.get('[value="email"]').click();

      cy.get("[name='contactEmail']").type("notanemail")

      cy.contains('Send Verification').click()
      cy.contains('Invalid email address');

    })
  })

  describe("Passes contact validation", () => {

    it("Passes email validation", () => {
      cy.get('[value="email"]').click();

      cy.get("[name='contactEmail']").type("dev-null@freshworks.io");

      cy.contains("Send Verification").click();

      cy.get("[data-cy='confirmationPanel']").contains("A link to your application form has been sent to your email");
      // cy.contains("A link to your application form has been sent to your email")
    })

    it("Passes call-in validation", () => {

      cy.contains('I don’t have access to either of these').click()

      cy.contains("First name* (primary contact)")
    })

  })
})
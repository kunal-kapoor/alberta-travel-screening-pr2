import { passContactStep } from '../../utils/utils';

describe("Fails email validation", () => {
  beforeEach(() => {
    // reset and pass eligibility validation
    cy.visit("http://localhost:3000/enroll");
    passContactStep();
  })

  it("Fails email pattern validation", () => {
    cy.get("[name='email']").type("notanemail").blur();

    cy.contains('Invalid email address');
  })

  it("Fails Email confirmation validation due to mismatch", () => {
    cy.get("[name='email']").type("dev-null@freshworks.io");

    cy.get("[name='confirmEmail']").type("wrongEmail@freshworks.io").blur();

    cy.contains('Email fields must match');
  })

  it("Fails Email confirmation validation due to missing confirmation email", () => {
    cy.get("[name='email']").type("dev-null@freshworks.io");

    cy.get("[name='confirmEmail']").focus().blur();

    cy.contains('Must confirm email');
  })
})
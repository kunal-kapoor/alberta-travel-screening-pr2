import { 
  passContactStep, 
  fillPrimaryContact, 
  fillArrivalInformation, 
  fillIsolationQuestionnaire, 
  fillAdditionalTravellers 
} from '../../utils/utils';

describe("Passes enrollment validation", () => {

  it("Passes enrollment validation - all dynamic fields", () => {
    // reset and pass eligibility validation
    cy.visit("http://localhost:3000/enroll");
    passContactStep();

    //Fill primary contact portion
    fillPrimaryContact();

    // Marks form primary as exempt
    cy.get('[value="Exempt"]').click();
    cy.get('[id="mui-component-select-exemptOccupation"]').click();
    cy.get('[data-value="Crew member (air)"]').click();
    cy.get('[id="mui-component-select-exemptOccupationDetails"]').click();
    cy.get('[data-value="Air transportation"]').click();

    //Fill arrival information portion
    fillArrivalInformation();

    //Marks additional members and travellers outside household as No
    cy.get('[name="hasAdditionalTravellers"]').check("Yes");
    cy.get('[id="mui-component-select-numberOfAdditionalTravellers"]').click();
    cy.get('[data-value="2"]').click();

    fillAdditionalTravellers({numberOfFieldSets: 2, omitOne: false});


    //Fill isolation questionnaire portion, pass omit flag to not fill one field
    fillIsolationQuestionnaire({omitOne: false});

    cy.contains("Continue").click();

    cy.contains("Your application will be verified upon entry into Canada. Please make note of your confirmation number(s).")
  })
})
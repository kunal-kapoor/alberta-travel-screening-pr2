{
  "swagger" : "2.0",
  "info" : {
    "description" : "Alberta Health API",
    "version" : "1.0.0",
    "title" : "Alberta Health Travel Screening"
  },
  "tags" : [ {
    "name" : "AHS",
    "description" : "Operations for travellers"
  }, {
    "name" : "Admin portal",
    "description" : "Operations performed by Alberta Health"
  } ],
  "securityDefinitions" : {
    "accessCode" : {
      "type" : "oauth2",
      "tokenUrl" : "https://us-south.appid.cloud.ibm.com/oauth/v4/437364da-8620-42a6-8d22-c3a4b8ffe6e2",
      "authorizationUrl" : "https://us-south.appid.cloud.ibm.com",
      "flow" : "accessCode",
      "scopes" : {
        "write" : "allows modifying resources",
        "read" : "allows reading resources"
      }
    }
  },
  "security" : [ {
    "accessCode" : [ ]
  } ],
  "paths" : {
    "/form" : {
      "get" : {
        "tags" : [ "AHS" ],
        "summary" : "returns welcome page",
        "operationId" : "getForm",
        "description" : "Returns a welcome message",
        "produces" : [ "application/string" ],
        "responses" : {
          "200" : {
            "description" : "Welcome to Alberta Health ETS"
          }
        }
      },
      "post" : {
        "tags" : [ "AHS" ],
        "summary" : "submits a new form",
        "operationId" : "submitForm",
        "description" : "Submits a new form filled by a traveller",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "in" : "body",
          "name" : "travellerForm",
          "description" : "Form submited by a traveller",
          "schema" : {
            "$ref" : "#/definitions/TravellerForm"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "Valid input",
            "schema" : {
              "type" : "object",
              "properties" : {
                "message" : {
                  "type" : "string",
                  "example" : "Form submission successful"
                },
                "confirmationNumber" : {
                  "type" : "string",
                  "description" : "Traveller Confirmation number",
                  "example" : "AC0CB4A5"
                }
              }
            }
          },
          "400" : {
            "description" : "Invalid input"
          }
        }
      }
    },
    "/admin/lastname/{lname}" : {
      "get" : {
        "tags" : [ "Admin portal" ],
        "summary" : "returns a form submission",
        "operationId" : "getByLastName",
        "description" : "Searches for submissions by last name",
        "parameters" : [ {
          "in" : "path",
          "name" : "lname",
          "type" : "string",
          "required" : true,
          "description" : "Last name of a traveller who submitted a form"
        } ],
        "produces" : [ "application/json" ],
        "responses" : {
          "200" : {
            "description" : "Submission was found",
            "schema" : {
              "$ref" : "#/definitions/TravellerForm"
            }
          },
          "404" : {
            "description" : "No last name parameter given / no submissions found"
          }
        }
      }
    },
    "/form/{confirmationNumber}" : {
      "get" : {
        "tags" : [ "Admin portal" ],
        "summary" : "returns a form submission",
        "operationId" : "getByNumber",
        "description" : "Searches for submissions by confirmation number",
        "parameters" : [ {
          "in" : "path",
          "name" : "confirmationNumber",
          "type" : "string",
          "required" : true,
          "description" : "Confirmation number of a submitted form"
        } ],
        "produces" : [ "application/json" ],
        "responses" : {
          "200" : {
            "description" : "Submission was found",
            "schema" : {
              "$ref" : "#/definitions/TravellerForm"
            }
          },
          "404" : {
            "description" : "No last name parameter given / no submissions found"
          }
        }
      }
    },
    "/db/form/{id}" : {
      "get" : {
        "tags" : [ "Admin portal" ],
        "summary" : "returns a form submission by id",
        "operationId" : "getById",
        "description" : "Searches for submissions by primary key",
        "parameters" : [ {
          "in" : "path",
          "name" : "id",
          "type" : "number",
          "required" : true
        } ],
        "produces" : [ "application/json" ],
        "responses" : {
          "200" : {
            "description" : "Submission was found",
            "schema" : {
              "$ref" : "#/definitions/TravellerForm"
            }
          },
          "404" : {
            "description" : "No last name parameter given / no submissions found"
          }
        }
      }
    }
  },
  "definitions" : {
    "TravellerForm" : {
      "type" : "object",
      "required" : [ "firstName", "lastName", "dateOfBirth", "phoneNumber", "hasAdditionalTravellers", "arrivalDate", "arrivalCityOrTown", "arrivalCountry", "hasPlaceToStayForQuarantine", "isAbleToMakeNecessaryArrangements" ],
      "properties" : {
        "firstName" : {
          "type" : "string",
          "description" : "First name of a primary contact",
          "example" : "Jack"
        },
        "lastName" : {
          "type" : "string",
          "description" : "Last name of a primary contact",
          "example" : "Johnson"
        },
        "dateOfBirth" : {
          "type" : "string",
          "description" : "Date of birth of a traveller",
          "example" : "1970/05/15"
        },
        "phoneNumber" : {
          "type" : "string",
          "description" : "Phone number of a primary contact",
          "example" : "1 780 555 5555"
        },
        "email" : {
          "type" : "string",
          "description" : "Email of a traveller",
          "example" : "jack@mail.com"
        },
        "hasAdditionalTravellers" : {
          "type" : "string",
          "description" : "Specifies whether a traveller enters the country in a group",
          "example" : "yes"
        },
        "additionalTravellers" : {
          "type" : "array",
          "description" : "List of additional travellers if any",
          "items" : {
            "$ref" : "#/definitions/AdditionalTraveller"
          }
        },
        "nameOfAirportOrBorderCrossing" : {
          "type" : "string",
          "description" : "Name of Airport or Border Crossing",
          "example" : "LO5067"
        },
        "arrivalDate" : {
          "type" : "string",
          "description" : "Traveller's arrival date",
          "example" : "2020/06/01"
        },
        "arrivalCityOrTown" : {
          "type" : "string",
          "description" : "Traveller's arriving from city name",
          "example" : "Vancouver"
        },
        "arrivalCountry" : {
          "type" : "string",
          "description" : "Traveller's arriving from country name",
          "example" : "Canada"
        },
        "additionalCitiesAndCountries" : {
          "type" : "array",
          "description" : "List of additional loactions visited by a traveller in the past 14 days",
          "items" : {
            "$ref" : "#/definitions/VisitedLocation"
          }
        },
        "hasPlaceToStayForQuarantine" : {
          "type" : "string",
          "description" : "Specifies whether a traveller has a place to stay during the required quarantine or isolation period",
          "example" : "yes"
        },
        "quarantineLocation" : {
          "type" : "object",
          "description" : "Quarantine location contact information if applicable",
          "allOf" : [ {
            "$ref" : "#/definitions/QuarantineLocation"
          } ]
        },
        "isAbleToMakeNecessaryArrangements" : {
          "type" : "string",
          "description" : "Specifies whether a traveller is able to make the necessary arrangements during the applicable quarantine or isolation period",
          "example" : "yes"
        }
      }
    },
    "AdditionalTraveller" : {
      "type" : "object",
      "properties" : {
        "firstName" : {
          "type" : "string",
          "description" : "Name of a person who accompanies a traveller",
          "example" : "Jill"
        },
        "lastName" : {
          "type" : "string",
          "description" : "Last name of a person who accompanies a traveller",
          "example" : "Johnson"
        },
        "dateOfBirth" : {
          "type" : "string",
          "description" : "Date of birth of a person who accompanies a traveller",
          "example" : "1972/03/14"
        }
      }
    },
    "VisitedLocation" : {
      "type" : "object",
      "properties" : {
        "cityOrTown" : {
          "type" : "string",
          "description" : "Name of a city visited by a traveller in the past 14 days",
          "example" : "San Jose"
        },
        "country" : {
          "type" : "string",
          "description" : "Name of a city country by a traveller in the past 14 days",
          "example" : "United States"
        }
      }
    },
    "QuarantineLocation" : {
      "type" : "object",
      "properties" : {
        "address" : {
          "type" : "string",
          "description" : "Address of a place where a traveller will be quarantining or isolating",
          "example" : "7 Bunting Hill"
        },
        "cityOrTown" : {
          "type" : "string",
          "description" : "City where a traveller will be quarantining or isolating",
          "example" : "Edmonton"
        },
        "provinceTerritory" : {
          "type" : "string",
          "description" : "Province where a traveller will be quarantining or isolating",
          "example" : "Alberta"
        },
        "postalCode" : {
          "type" : "string",
          "description" : "Postal code of a place where a traveller will be quarantining or isolating",
          "example" : "T6H 2X6"
        },
        "phoneNumber" : {
          "type" : "string",
          "description" : "Contact information a place where a traveller will be quarantining or isolating",
          "example" : "1 123 444 5555"
        },
        "typeOfPlace" : {
          "type" : "string",
          "description" : "Type a place where a traveller will be quarantining or isolating",
          "example" : "Private residence"
        },
        "howToGetToPlace" : {
          "type" : "string",
          "description" : "Type transpotation a traveller will be taking to get to isolation place",
          "example" : "Taxi"
        },
        "otherPeopleResiding" : {
          "type" : "string",
          "description" : "Specifies whether there are other people living in t place where a traveller will be quarantining or isolating that are not in the travelling party",
          "example" : "no"
        },
        "doesVulnerablePersonLiveThere" : {
          "type" : "string",
          "description" : "Specifies whether a vulnerable person also lives in a place where a traveller will be quarantining or isolating",
          "example" : "no"
        }
      }
    }
  },
  "host" : "virtserver.swaggerhub.com",
  "schemes" : [ "https" ],
  "basePath" : "/goldamber/AlbertaHealth/1.0.0"
}
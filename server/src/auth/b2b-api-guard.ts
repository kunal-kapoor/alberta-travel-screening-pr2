import { Injectable } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";

@Injectable()
export class B2bAuthGuard extends AuthGuard('jwt') {}

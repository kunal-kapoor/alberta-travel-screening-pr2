import {
    ExecutionContext,
    Injectable,
    UnauthorizedException,
  } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { LoggerService } from "../../logs/logger";
import { Observable } from 'rxjs';
import { AppIDConfig } from '../envconfig';
import { nameOrEmail } from '../parsename';

@Injectable()
export class OthersGuard extends AuthGuard("others") {

  constructor() {
    const envConfig = AppIDConfig('OTHER');

    super({
      oauthServerUrl: (envConfig.OAUTH_SERVER_URL || 'aurl').trim(),
      tenantId: (envConfig.TENANT_ID ||'anid').trim(),
    });
  }

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    return super.canActivate(context)
  }

  handleRequest(err: any, user: any, info: any): any {
    if (err || !user) {
      // Login failed
      LoggerService.info(info);
      throw err || new UnauthorizedException();
    }

    user.name = nameOrEmail(user);
    
    return user;
  }
}

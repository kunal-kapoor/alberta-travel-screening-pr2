import { Injectable } from "@nestjs/common";
import { DbService } from '../db/dbservice';
import { AgentReportAccessRO, ReportAccessControlListRO } from "./ro/report-access.ro";
import { REPORTS } from "./constants";

@Injectable()
export class ACLRepository {

    /**
     * 
     * Inserts or updates the agent email and their report type access.
     * 
     * @param reportAccess - Agent Email Address and the Array of report types access is provided to.
     */
    async createorUpdateReportAccessForAgent(reportAccess: AgentReportAccessRO) {
        await reportAccess.reportName.forEach(async (reportName) => {
            /**
             * Checks if their exists an entry in the db for the current agent and the report type
             */
            const res = await DbService.getReportAccessStatusForAgent(reportAccess.agentEmail, reportName);
            if (!res || res.length == 0) {
                /**
                 * If there're no entry for the report type and agent combination, an entry is entered providing permission to the report type
                 */
                await DbService.createReportAccessForAgent(reportAccess.agentEmail, reportName);
            }
            else {
                /**
                 * In case of entry being available on the db, the values are updated
                 */
                await DbService.enableReportAccessForAgent(reportAccess.agentEmail, reportName);
            }
        });
        /**
         * Removes access to all types of reports that's not available in the current list
         */
        if (reportAccess.reportName.length === 0) {
            await DbService.revokeFullReportAccessForAgent(reportAccess.agentEmail);
        }
        else {
            await DbService.revokeReportAccessForAgent(reportAccess.agentEmail, reportAccess.reportName);
        }
    }

    /**
     * 
     * Returns Agent Email and Array of report type they have access.
     * 
     * @param agentEmail 
     */
    async findReportAccessByAgentEmail(agentEmail: string): Promise<any> {
        const res = await DbService.getActiveReportAccessForAgent(agentEmail);

        if (!res) {
            return [];
        }

        return new AgentReportAccessRO(res);
    }

    /**
     * Returns the entire list of Agents and the report types they have access.
     */
    async getAccessControlList(): Promise<ReportAccessControlListRO> {
        const res = await DbService.getReportAccessControlList();
        return new ReportAccessControlListRO(res);
    }

    /**
     * Returns the list of available Reports
     */
    getAvailableReportsList(): any  {
        return REPORTS;
    }
}
export const CHECKED_STATUSES = [
    'Contact made, no issues',
     //voicemail?
    'Contact made, would not provide info',
    'Contact made, essential/exempt worker',
    'Contact made, indicated unwilling to self-isolate',
    'No answer, 3 call attempts made',
    'Incorrect contact info provided',
    'Contact made, other',
]

export const AIRPORT_OR_BORDER_CROSSINGS = [
    'Airport - Edmonton International (YEG)',
    'Airport - Calgary International (YYC)',
    'Aden Border Crossing',
    'Carway Border Crossing',
    'Chief Mountain Border Crossing',
    'Coutts Border Crossing',
    'Waterton Lakes Park Border Crossing',
    'Wildhorse Border Crossing',
];

export const REPORTS: Readonly<any> = Object.freeze({
    'Checkpoint Report': 'checkpoint',
    'Service Alberta Report': 'serviceAlberta',
    'Call Count': 'callCount',
    'Monitoring Portal': 'monitoring-portal'
});

export const RTOP_AIRPORT_OR_BORDER_CROSSINGS = [
    'Airport - Calgary International (YYC)',
    'Coutts Border Crossing',
  ];
import { Exclude, Expose } from "class-transformer";
import { AgentDTO } from "../service-alberta.repository";

@Exclude()
export class AgentRO {

  @Expose()
  name: string

  constructor(agent: AgentDTO) {
    this.name = agent.name
  }
}
import { Exclude, Expose } from "class-transformer";
import moment from "moment-timezone";

/**
 * id
 * agent
 * status
 * note
 */
@Exclude()
export class ActivityRO {

  @Expose()
  date: string;

  @Expose()
  agent: string;

  @Expose()
  note: string;

  @Expose()
  type: string;

  @Expose()
  status: string;

  constructor(activity: ActivityRO) {
    activity.date = moment(activity.date).utc().format()
    Object.assign(this, activity);
  }
}

export class ServiceAlbertaFromQueryRO{
    id: number;
    confirmationNumber: string;
    agent: string;
    firstName:string;
    lastName:string;
    status: string;
    assignedToMe: boolean;
    arrivalDate:string;
    lastUpdated: Date;
    constructor(data:  ServiceAlbertaFromQueryRO) {
        (data);
    }
}

export class ServiceAlbertaFromQueryResultRO {
    numberOfResults: number;
    results: ServiceAlbertaFromQueryRO[];

    constructor(numberOfResults: number, results: ServiceAlbertaFromQueryRO[]) {
        this.numberOfResults = numberOfResults;
        this.results = results;
    }
}

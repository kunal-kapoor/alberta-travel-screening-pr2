import { ArrivalForm } from "../arrival-form/entities/arrival-form.entity";
import { query } from "./db2connection";
import { ArrivalFormRO } from "../arrival-form/ro/arrival-form.ro";
import { ServiceAlbertaFormRO } from "../service-alberta/ro/form.ro";
import { HttpException, HttpStatus } from "@nestjs/common";

/**
 * Provides utilities to transform results of ArrivalForm queries
 * to a form that's more easier to work with
 */
class ArrivalFormResultTransformer {
    constructor(private result) {}

    count = ():number => {
        const [{COUNT=0}] = this.result || [{COUNT: 0}];
        return COUNT;
    }

    /**
     * Returns a ArrivalFormDTO as the FORM_RECORD column of the first
     * row in the query results.
     */
    single = async ():Promise<ArrivalFormRO> => {
        const [form] = this.result || [null];
        if(form) {
            const record = JSON.parse(form['FORM_RECORD']);
            
            record.id = form['ID'];
            record.determinationDate = form['DETERMINATION_DATE'];
            return new ArrivalFormRO(record);
        } {
            throw new HttpException({message:"Record not found"}, HttpStatus.NOT_FOUND);
        }
    }

    singleRaw = async (): Promise<ArrivalForm> => {
        const [form] = this.result || [null];
        if(form) {
            const record = JSON.parse(form['FORM_RECORD']);
            record.id = form['ID'];
            record.determinationDate = form['DETERMINATION_DATE'];
            return record;
        } {
            throw new HttpException({message:"Record not found"}, HttpStatus.NOT_FOUND);
        }
    }

    singleService = async ():Promise<ServiceAlbertaFormRO> => {
        const [form] = this.result || [null];

        if(form) {
            const record = JSON.parse(form['FORM_RECORD']);
            record.agent = form['AGENTNAME'];
            record.agentId = form['AGENTID'];

            // For Service-Alberta purposes, the date of determination submission
            // should be considered as the travellers Arrival Date
            record.arrivalDate = form['DETERMINATION_DATE'];
            record.id = form['ID'];
            return new ServiceAlbertaFormRO(record);
        } {
            throw new HttpException({message:"Record not found"}, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Returns a list of ArrivalForms as the FORM_RECORD columns
     * from all the search results
     */
    multiple = async (): Promise<ArrivalForm[]> => {
        return this.result.map(({ID, FORM_RECORD,STATUS,OWNER,OWNERID, LAST_UPDATED, DETERMINATION_DATE}) => {
            const rec = JSON.parse(FORM_RECORD);
            rec.id = ID;
            rec.status = STATUS;
            rec.owner = OWNER;
            rec.lastUpdated = LAST_UPDATED;
            rec.assignedTO = OWNERID;
            rec.determinationDate = DETERMINATION_DATE;
            return rec;
        });
    }

    /**
     * Returns the id of the first row of the results.
     */
    idOnly = async (): Promise<number> => {
        const [{ID=null}] = this.result || [];

        return ID;
    }

    value = ():any => this.result;
}

export async function arrivalFormQuery(queryStr, queryArgs?) {
    const res = await query(queryStr, queryArgs);
    return new ArrivalFormResultTransformer(res);
}
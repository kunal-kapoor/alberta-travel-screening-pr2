import { LoggerService } from "../logs/logger";
import { MigrateLatest } from "./db2rtopmigrations";

/**
 * Utility function to run the latest DB migration to be applied
 */
(async () => {
    try {
        await MigrateLatest();
        LoggerService.info('Successfully migrated db');
    } catch(e) {
        LoggerService.error('Failed to migrate db', e);
    }
})();


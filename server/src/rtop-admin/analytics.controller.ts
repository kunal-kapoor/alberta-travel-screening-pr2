import {
  Controller,
  Param,
  UseInterceptors,
  Get,
  Req,
  HttpException,
  HttpStatus,
  ClassSerializerInterceptor,
  UseGuards,
  Query,
} from '@nestjs/common';
import { EnrollmentFormService } from '../rtop/enrollment-form.service';
import { PublicRoute } from '../decorators';
import { AuthGuard } from '@nestjs/passport';
import { LoggerService } from '../logs/logger';
import { ActionName } from '../logs/enums';
import moment from 'moment';
import { DATE_FORMAT, USER_TIMEZONE, DATE_TIME_FORMAT } from '../rtop/enrollment-form.constants';
import { EnrollmentStatus } from '../rtop/entities/household.entity';
import { RTopMonitoringRepository } from '../rtop/repositories/rtop-monitoring.repository';
import { TravellerStatusRO } from '../rtop/ro/traveller-status.ro';
import { AnalyticsAPIAuthGuard } from '../auth/analytics-api-jwt-guard';

const throw404 = (message = 'Invalid date format') => {
  throw new HttpException({
    status: HttpStatus.NOT_FOUND,
    error: message,
  }, HttpStatus.NOT_FOUND);
}

@Controller('/api/v2/rtop/analytics')
@UseGuards(AnalyticsAPIAuthGuard)
export class AnalyticsApiController {

  constructor(
    private readonly monitoringPortalRepository: RTopMonitoringRepository
  ) { }

  private getParsedDate(date: string) {
    if (moment(date, DATE_FORMAT, true).isValid()) {
      return moment.tz(date, DATE_FORMAT, USER_TIMEZONE).utc().format(DATE_TIME_FORMAT);
    }
    else if (moment(date, moment.ISO_8601, true).isValid()) {
      return moment(date).utc().format(DATE_TIME_FORMAT);
    }
    else {
      throw404();
    }
  }

  @PublicRoute()
  @UseInterceptors(ClassSerializerInterceptor)
  @Get('daily-checkin')
  async getAllDailyCheckinForDate(@Req() req, @Query('date') date: string): Promise<any> {
    LoggerService.logData(req, ActionName.DAILY_CHECKIN_API, 'Daily-checkin', [date]);

    let dailySubmission: any;
    if (date) {
      const fromDate = this.getParsedDate(date);
      const toDate = moment(fromDate, DATE_TIME_FORMAT).tz(USER_TIMEZONE).endOf('day').utc().format(DATE_TIME_FORMAT);
      dailySubmission = await this.monitoringPortalRepository.getAllDailyCheckinForDate(fromDate, toDate);
    }
    else {
      dailySubmission = await this.monitoringPortalRepository.getAllDailyCheckin();
    }
    return dailySubmission;
  }

  @PublicRoute()
  @UseInterceptors(ClassSerializerInterceptor)
  @Get('enrolled')
  async getEnrolledTravellers(@Req() req, @Query('date') date: string): Promise<any> {
    LoggerService.logData(req, ActionName.ENROLLMENT_STATUS_API, EnrollmentStatus.ENROLLED, [date]);

    let travellers: TravellerStatusRO[];
    if (date) {
      const fromDate = this.getParsedDate(date);
      const toDate = moment(fromDate, DATE_TIME_FORMAT).tz(USER_TIMEZONE).endOf('day').utc().format(DATE_TIME_FORMAT);
      travellers = await this.monitoringPortalRepository.getTravellersStatusForDate(fromDate, toDate, EnrollmentStatus.ENROLLED);
    }
    else {
      travellers = await this.monitoringPortalRepository.getTravellersStatus(EnrollmentStatus.ENROLLED);
    }
    return { travellers };
  }

  @PublicRoute()
  @UseInterceptors(ClassSerializerInterceptor)
  @Get('withdrawn')
  async getWithdrawnTravellers(@Req() req, @Query('date') date: string): Promise<any> {
    LoggerService.logData(req, ActionName.ENROLLMENT_STATUS_API, EnrollmentStatus.WITHDRAWN, [date]);

    let travellers: TravellerStatusRO[];
    if (date) {
      const fromDate = this.getParsedDate(date);
      const toDate = moment(fromDate, DATE_TIME_FORMAT).tz(USER_TIMEZONE).endOf('day').utc().format(DATE_TIME_FORMAT);
      travellers = await this.monitoringPortalRepository.getTravellersStatusForDate(fromDate, toDate, EnrollmentStatus.WITHDRAWN);
    }
    else {
      travellers = await this.monitoringPortalRepository.getTravellersStatus(EnrollmentStatus.WITHDRAWN);
    }
    return { travellers };
  }
}

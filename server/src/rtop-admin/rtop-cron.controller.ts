import {
  Controller,
  UseInterceptors,
  ClassSerializerInterceptor,
  Patch,
  UseGuards,
} from '@nestjs/common';
import { LoggerService } from '../logs/logger';
import { PublicRoute } from '../decorators';
import { DailyQuestionnaireService } from './daily-reminder.service';
import { DailyCheckService } from './daily-check.service';
import { AuthGuard } from '@nestjs/passport';
import moment from 'moment';
import { SlackNotifierService } from '../slack-notifier.service';
import { DailyStatsService } from '../rtop/daily-stats.service';

/**
 * This controller contains endpoints that will be used by an internal
 * cron job to trigger various jobs.
 */
@Controller('/api/v2/rtop-admin/cron')
export class RTOPCronController {

  constructor(
    private readonly questionnaireService: DailyQuestionnaireService,
    private readonly dailyCheckService: DailyCheckService,
    private readonly slackNotifierService: SlackNotifierService,
    private readonly dailyStatsService: DailyStatsService,
  ) { }

  /**
   * Send daily reminders
   */
  // Disable global guards
  @PublicRoute()
  // Enable jwt auth guard
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(ClassSerializerInterceptor)
  @Patch('/send-daily-checkin')
  async runJob(): Promise<void> {
    try {
      LoggerService.info(`Sending Daily Check-in cron job started at ${moment().format()}`);
      const [total, success, failed] = await this.questionnaireService.sendDailyReminders();
      LoggerService.info(`Sending Daily Check-in cron job ended at ${moment().format()}`);
      this.slackNotifierService.postMessage(`Sending Daily Check-in Job succeeded with a total= ${total}, success= ${success}, failed= ${failed}.`);
    } catch (e) {
      LoggerService.error('Failed to Send daily checkin', e);

      throw e;
    }
  }

  /**
   * Send daily reminders for call in travellers
   */
  // Disable global guards
  @PublicRoute()
  // Enable jwt auth guard
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(ClassSerializerInterceptor)
  @Patch('/send-daily-call-in')
  async runDailyJob(): Promise<void> {
    try {
      LoggerService.info(`Daily Call-in cron job started at ${moment().format()}`);
      await this.questionnaireService.sendCallInDailyReminders();
      LoggerService.info(`Daily Call-in cron job ended at ${moment().format()}`);
    } catch (e) {
      LoggerService.error('Failed to edit form', e);

      throw e;
    }
  }

  /**
   * Update statuses of households/travellers based on if they
   * have completed their daily check in etc.
   * 
   * Scheduler runs 11.59 PM MT daily - which is the next day in UTC.
   * No issue will occur running the scheduler on the next day UTC time.
   * 
   */
  // Disable global guards
  @PublicRoute()
  // Enable jwt auth guard
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(ClassSerializerInterceptor)
  @Patch('/daily-check')
  async runDailyCheck(): Promise<void> {
    try {
      LoggerService.info(`Daily Check cron job started at ${moment().format()}`);
      const res = await this.dailyCheckService.checkTokens();
      if (res) {
        LoggerService.info(`Daily Check cron job ended at ${moment().format()}`);
        this.slackNotifierService.postMessage(`Daily Check cron job.`);
      } else {
        LoggerService.info(`Daily Check cron job failed at ${moment().format()}`);
      }
    } catch (e) {
      LoggerService.error('Failed to run daily check', e);
      throw e;
    }
  }

  /**
   * Update statuses of households/travellers based on if they
   * have completed their 2nd covid test by day 9.
   * 
   */
  // Disable global guards
  @PublicRoute()
  // Enable jwt auth guard
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(ClassSerializerInterceptor)
  @Patch('/second-test-check')
  async runSecondTestCheck(): Promise<void> {
    try {
      LoggerService.info(`Daily Second Test Check cron job started at ${moment().format()}`);
      const numUpdated: number = await this.dailyCheckService.checkTravellerSecondTests();
      LoggerService.info(`Daily Second Test cron job ended at ${moment().format()}`);
      this.slackNotifierService.postMessage(`Daily Second Test cron job succeeded. ${numUpdated} records were marked Yellow.`);
    } catch (e) {
      LoggerService.error('Failed to run Daily Second Test', e);
      throw e;
    }
  }


  @PublicRoute()
  // Enable jwt auth guard
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(ClassSerializerInterceptor)
  @Patch('/daily-checkin-reminder')
  async runDailyCheckReminder(): Promise<void> {
    try {
      LoggerService.info(`Daily Check-in reminder cron job started at ${moment().format()}`);
      const [total, success, failed] = await this.questionnaireService.sendCheckInReminders();
      LoggerService.info(`Daily Check-in reminder cron job ended at ${moment().format()}`);
      this.slackNotifierService.postMessage(`Daily Check-in reminder succeeded with a total= ${total}, success= ${success}, failed= ${failed}.`);
    } catch (e) {
      LoggerService.error('Failed to run daily check', e);
      throw e;
    }
  }

  @PublicRoute()
  // Enable jwt auth guard
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(ClassSerializerInterceptor)
  @Patch('/daily-stats')
  async runDailyStats(): Promise<void> {
    try {
      LoggerService.info(`Daily stats started at ${moment().format()}`);
      await this.dailyStatsService.dailyEnrollmentStats();
      LoggerService.info(`Daily stats ended at ${moment().format()}`);
      this.slackNotifierService.postMessage(`Daily stats sent.`);
    } catch (e) {
      LoggerService.error('Failed to run stats', e);
      this.slackNotifierService.postMessage(`Daily stats failed.`);
      throw e;
    }
  }
}

import { Injectable } from "@nestjs/common";
import moment from "moment";
import { RTOPDbService } from "../../db/rtop-dbservice";
import { EnrollmentStatus } from "../../rtop/entities/household.entity";
import { DATE_FORMAT } from "../../rtop/enrollment-form.constants";
import { EnrollmentFormRepository } from "../../rtop/repositories/enrollment-form.repository";
import { DAILY_STATUS, CARD_STATUS_REASON } from "../../rtop/constants";
import { DailyCheckQuery } from "./daily-check.query";


export class DailyCheckRO {
    travellerId: number;
    travellerConfirmationNumber: string;

    constructor(data: any) {
        this.travellerId = data["TRAVELLER_ID"];
        this.travellerConfirmationNumber = data["CONFIRMATION_NUMBER"];
    }
}

@Injectable()
export class DailyCheckRepository {

    constructor(
        private readonly enrollmentFormRepository: EnrollmentFormRepository
    ) { }

    async updateCompletedHouseholdStatus(): Promise<any> {
        let householdIds = await DailyCheckQuery.updateCompletedHouseholdStatus();

        if (!householdIds || householdIds.length < 1) {
            return;
        }
        householdIds = householdIds.map((t) => t.ID);

        await DailyCheckQuery.updateCompletedTravellerStatus(householdIds);
    }

    /**
     * Updates card status of travellers/household that have not
     * performed their second test by day 9
     */
    async checkTravellerSecondTest(): Promise<number> {
        const missedTravellers = (await RTOPDbService.checkTravellerSecondTest())
            .map(el => new DailyCheckRO(el));

        await this.updateCardStatusForTravellers(missedTravellers, CARD_STATUS_REASON.NOT_TESTED);

        return missedTravellers.length;
    }

    /**
     * Updates card status of households that have travellers that did not
     * complete their daily checkin yesterday 
     */
    async updateExpiredStatus(): Promise<any> {
        const yesterdayDate = moment().subtract('1', 'day').format(DATE_FORMAT);
        /**
         * fetches all the travellers who have not submitted their daily survey
         */
        const expiredTravellers: DailyCheckRO[] = (await DailyCheckQuery.getExpiredTravellerStatus(yesterdayDate)).map((ele) => {
            return new DailyCheckRO(ele);
        });
        
        await this.updateCardStatusForTravellers(expiredTravellers, CARD_STATUS_REASON.DAILY_TRACKING_FLAG);
    }

    private async updateCardStatusForTravellers(travellers: DailyCheckRO[], reason: CARD_STATUS_REASON) {
        const travellerIds: number[] = travellers.map((ele) => ele.travellerId);
        const travellerConfirmationNumbers: string[] = travellers.map((ele) => ele.travellerConfirmationNumber);
        /**
         * Update card status of travellers to YELLOW and update card status of associated Household
         */
        if (travellerIds.length > 0) {
            const status: string = DAILY_STATUS.YELLOW;
            await this.enrollmentFormRepository.updateHouseholdCardStatus(travellerConfirmationNumbers, status, reason, null);
            await this.enrollmentFormRepository.updateTravellerCardStatus(travellerIds, status);
        }
    }
}

import { Injectable } from "@nestjs/common";
import moment from "moment";
import { RTOPDbService } from "../../db/rtop-dbservice";
import { VerificationTokenService } from "../../rtop/verification-token.service";
import { DATE_FORMAT, USER_TIMEZONE } from "../../rtop/enrollment-form.constants";
import { EnrollmentStatus } from "../../rtop/entities/household.entity";
import { DailyCheckQuery } from "./daily-check.query";

export enum NOTIFICATION_STATUS {
    NOT_SENT = 'not_sent',
    SENT = 'sent',
    FAILED = 'failed'
}

export enum CONTACT_METHOD {
    SMS = 'sms',
    EMAIL = 'email',
    CALLIN = 'callIn',
}

export class DailyReminderRO {
    id: string;
    email: string;
    phoneNumber: string;
    token: string;
    smsStatus: string;
    emailStatus: string;
    contactMethod: CONTACT_METHOD;
    date: string;
    
    constructor(data) {
        this.id = data['ID'];

        this.email = data['EMAIL'] || data['FORM_EMAIL'];
        this.phoneNumber = data['PHONE_NUMBER'];
        this.token = data['TOKEN'];
        this.smsStatus = data['SMS_STATUS'];
        this.emailStatus = data['EMAIL_STATUS'];
        this.date = data['DATE'];
        this.contactMethod = data['CONTACT_METHOD'];
    }
}

@Injectable()
export class DailyReminderRepository {
    /**
     * Create reminder records for all households that doesn't already
     * have a record for today
     * 
     * Optionally only create reminder for the given household
     */
    async createReminders(householdId?: number) {
        const today = moment().tz(USER_TIMEZONE).format(DATE_FORMAT);

        // List households that do not have a reminder for today
        const households = await RTOPDbService.listNotCreatedReminderHouseholds(today, householdId);

        if(!households.length) {
            return;
        }
        
        const token = () => VerificationTokenService.createToken(48); // Random string of 48 bytes
        
        // Construct query to insert missing reminders
        const toCreate = households.map(f => [today, f['ID'], 'created', token(),  NOTIFICATION_STATUS.NOT_SENT, NOTIFICATION_STATUS.NOT_SENT]);

        // Create new reminders
        await RTOPDbService.createDailyReminders(toCreate);
    }

    /**
     * List unset daily reminders from today.
     * 
     * @param date The date to filter by
     */
    async listUnsentReminders(date: string): Promise<DailyReminderRO[]> {
        const reminders = await RTOPDbService.listUnsentDailyReminders(date);

        return reminders.map(d => new DailyReminderRO(d));
    }

    /**
     * Lists call in reminders for the given day
     * @param date - the date to list unsent reminders for
     */
    async listCallInDailyReminders(date: string): Promise<DailyReminderRO[]> {
        const reminders = await RTOPDbService.listCallInDailyReminders(date);

        return reminders.map(d => new DailyReminderRO(d));
    }

    /**
     * Update the email sending status associated with the given reminder
     * @param reminderId reminder to update
     * @param status status of reminder to update
     */
    async updateEmailStatus(reminderId: string, status: NOTIFICATION_STATUS): Promise<void> {
        await RTOPDbService.updateReminderEmailStatus(reminderId, status);
    }

    /**
     * Update the SMS sending status associated with the given reminder
     * @param reminderId reminder to update
     * @param status status of reminder to update
     */
    async updateSmsStatus(reminderId: string, status: NOTIFICATION_STATUS): Promise<void> {
        await RTOPDbService.updateReminderSMSstatus(reminderId, status);
    }

    /**
     * Retrieve todays reminder for the given household
     * @param householdId The household id to retrieve reminder for
     * @param date The date to get reminder for
     */
    async getTodaysReminder(householdId: number, date: string): Promise<DailyReminderRO> {
        const reminders = await RTOPDbService.getTodaysReminder(householdId, date);

        return reminders && reminders.length && new DailyReminderRO(reminders[0]);
    }

    /**
     * 
     * Retrieves all the travellers who has not submitted their daily submission
     * Returns grouped reminders based on household/daily_reminder_id
     * @param date 
     */
    async getUnfilledHousholdReminder(date: string): Promise<DailyReminderRO[]> {
        const reminders = await DailyCheckQuery.getExpiredTravellerStatus(date);
        const groupByHousehold = {};
        reminders.map(d => groupByHousehold[d['ID']] = d );
        return Object.values(groupByHousehold).map((d) => new DailyReminderRO(d));
    }

    async getReminderInformationFromToken(token: string): Promise<any> {
        return await RTOPDbService.getReminderInfoFromToken(token);
    }
}

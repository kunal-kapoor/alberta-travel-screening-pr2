import { Injectable } from "@nestjs/common";
import { LoggerService } from '../logs/logger';
import { RTOPDbService } from "../db/rtop-dbservice";
import { DailyCheckRepository } from "./repositories/daily-check.repository";


@Injectable()
export class DailyCheckService {
    private static readonly JOB_NAME = 'daily-check';
    private static readonly SECOND_TEST_JOB_NAME = '2nd-test-daily-check';

    constructor(
        private readonly repo: DailyCheckRepository,
      ) {
    }

    /**
     * Run though all daily reminders and find which ones do not have submissions.
     * Change status to yellow for those submissions.
     */
    async checkTokens(): Promise<boolean> {
        LoggerService.info(`Job '${DailyCheckService.JOB_NAME}' is set to start`);

        try {
            await this.repo.updateExpiredStatus();
        } catch(e) {
            LoggerService.error('Failed to update expired status', e);
            return false;
        }
        try {
            await this.repo.updateCompletedHouseholdStatus();
        } catch(e) {
            LoggerService.error('Failed to update completed status', e);
            return false;
        }

        return true;
    }

    async checkTravellerSecondTests(): Promise<number> {
        LoggerService.info(`Job '${DailyCheckService.SECOND_TEST_JOB_NAME}' is set to start`);
        return await this.repo.checkTravellerSecondTest();
    }
  
}

import { IsString, IsIn, IsDate } from 'class-validator';
import { Type } from 'class-transformer';

export class DailyMonitoringExportReport {
    @IsString()
    @IsIn(["enrolled", "withdrawn"])
    type: string;
  
    @IsString()
    date: string;
}
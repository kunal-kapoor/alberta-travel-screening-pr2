import {
    IsOptional,
    IsString,
    IsBoolean,
    Length,
    Matches,
    Validate,
    IsIn,
} from 'class-validator';

import { Exclude } from 'class-transformer';
import { TransformBooleanString, IsNotFutureDate } from '../../decorators';
import { CONTACT_METHOD } from '../repositories/daily-reminder.repository.';

export class HouseholdTraveller {

    @IsOptional()
    @IsString()
    @Length(1, 255)
    firstName: string;

    @IsOptional()
    @IsString()
    @Length(1, 255)
    lastName: string;

    @IsOptional()
    @IsString()
    @Validate(IsNotFutureDate)
    dateOfBirth: Date;

    @IsOptional()
    @IsString()
    @Length(0, 35)
    phoneNumber: string;

    @IsOptional()
    @IsString()
    @Matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$|$^/)// eslint-disable-line no-useless-escape
    email: string;

    @IsOptional()
    @IsString()
    @Length(1, 250)
    gender: string;

    @IsOptional()
    @IsString()
    @Length(1, 1024)
    address: string;

    @IsOptional()
    @IsString()
    @Length(1, 255)
    cityOrTown: string;

    @IsOptional()
    @IsString()
    @Length(0, 32)
    postalCode: string;
}

import { IsArray, IsOptional, ValidateNested, IsString, Length } from "class-validator";
import { Type } from "class-transformer";

export class PerformedTestRecordDTO {
    @IsString()
    @Length(1, 30)
    confirmationNumber: string;

    @IsString()
    @Length(1, 55)
    testedAt: string;
}

export class PerformedTestsRecordsDTO {
    @IsArray()
    @ValidateNested({ each: true})
    @Type(() => PerformedTestRecordDTO)
    results: PerformedTestRecordDTO[];
}

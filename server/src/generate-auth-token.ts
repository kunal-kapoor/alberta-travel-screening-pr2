import jwt from 'jsonwebtoken';
import fs from 'fs';

// Utility function to generate an auth token for testing the AHS api endpoints
const privateKey = fs.readFileSync('../ahs-dev-api-priv.pem');

const token = jwt.sign(
    {},
    privateKey,
    {algorithm: 'RS256', expiresIn: '432000s'}
);

console.log(token);

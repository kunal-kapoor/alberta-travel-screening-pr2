import { DAILY_QUESTIONS, DAILY_STATUS, CARD_STATUS_REASON, DAILY_CHECK_OTHERS } from "./constants";
import { DailySubmissionAnswer } from "./enrollment-form.constants";
import { DailySubmission } from "./entities/daily-submission.entity";
import crypto from "crypto";
import { ReminderIDRO } from "./ro/reminder-id.ro";

export interface SubmissionCardStatus {
    STATUS: DAILY_STATUS,
    FLAG: CARD_STATUS_REASON
}

export class DailyQuestionAnswerService {

    /**
     * Returns red if any symptoms in submission were answered yes or other criterias were answered no,
     * otherwise green
     */
    public static submissionCardStatus(submission: DailySubmission): SubmissionCardStatus {
        const resultStatusObj = {
            STATUS: DAILY_STATUS.GREEN,
            FLAG: null
        };
        submission.answers?.forEach((a) => {
            if (DAILY_CHECK_OTHERS.includes(a.question)) {
                // Note: Flag check for ABTraceTogether question has been disabled on request from Product Owner
                // if (a.answer.toLowerCase() === 'no') {
                //     resultStatusObj.STATUS = DAILY_STATUS.RED;
                //     resultStatusObj.FLAG = resultStatusObj.FLAG ?? CARD_STATUS_REASON.ABTRACETOGETHER_FLAG;
                // }
            } else if (a.question in DAILY_QUESTIONS && a.answer.toLowerCase() === 'yes') {
                resultStatusObj.STATUS = DAILY_STATUS.RED
                resultStatusObj.FLAG = CARD_STATUS_REASON.DAILY_SYMPTOM_FLAG;
            }
        });
        return resultStatusObj;
    }

    public static hashAnswer(answerKey: string, reminder: ReminderIDRO) {

        if(!answerKey || !reminder.token || !reminder.travellerId) {
            // Fail hard if something is missing when hashing answer
            const reasons = []

            if(!answerKey) {reasons.push('Answer Key')}
            if(!reminder.token) {reasons.push('Token')}
            if(!reminder.travellerId) {reasons.push('Traveller ID')}

            throw new Error(`Missing required param(s) for answer hashing: ${reasons.join(', ')}`);
        }

        return crypto.createHash('sha256')
            .update(`${reminder.token}${answerKey}${reminder.travellerId}`)
            .digest('hex');
    }


     /**
     * Encode answers based on the given submission, encode with keys found in DAILY_QUESTIONS
     *  [{question: 'Are you following the rules?', answer: 'Yes'}] => ['abnwejkfrenw', 'sdjnfjnrwbnv']
     */
    public static encodeAnswers(submission: DailySubmission, reminder: ReminderIDRO): string[] {
        return submission.answers.map(item => {
            //Certain answers are text field and are not health information - which need not be encoded 
            if (item.question in DAILY_QUESTIONS) {
                const answer = item.answer.toLowerCase();
                const answerKey = answer === 'yes' ? DAILY_QUESTIONS[item.question].yes : DAILY_QUESTIONS[item.question].no;

                return this.hashAnswer(answerKey, reminder);
            }
            else {
                // Instead of encoding those items, it is added directly as strings
                return JSON.stringify(item);
            }
        });
    }

    /**
     * Find actual questions / answers based on decoded answers
     * ['abnwejkfrenw', 'sdjnfjnrwbnv'] => [{question: 'Are you following the rules?', answer: 'Yes'}]
     */
    public static decodeAnswers(answers: string[], reminder: ReminderIDRO): DailySubmissionAnswer[] {
        const notDecodedAnswers = [...answers];
        const decodedAnswers: any = Object.entries(DAILY_QUESTIONS).map(([question, ans]) =>  {
            return Object.entries(ans).map(([answer, key]) => {
                key = this.hashAnswer(key, reminder);
 
                if(answers.includes(key)) {
                    notDecodedAnswers.splice(notDecodedAnswers.indexOf(key), 1);
                    return {question, answer};
                }

                return null;
            });
        });

        //Fields that are not decoded are returned as such
        notDecodedAnswers.forEach((a) => decodedAnswers.push(JSON.parse(a)));
        return decodedAnswers.flat().filter(a => !!a);
    }
}
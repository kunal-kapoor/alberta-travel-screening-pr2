import { Injectable } from "@nestjs/common";
import { EmailService, EmailSendable } from "../email/email.service";
import { LoggerService } from "../logs/logger";
import { EnrollmentFormTravellerRO } from "./ro/form-submission.ro";

class EnrollmentConfirmationEmail implements EmailSendable {
    from: string;
    to: any;
    subject: string;
    template = 'enrollment-confirmation';
    context: any;

    constructor(to: string, data: { travellers: string }) {
        this.to = to;
        this.from = process.env.DAILY_REMINDER_EMAIL_SENDER;
        this.context = data;
        this.subject = 'Alberta Border Pilot: Application Confirmation';
    }
}


@Injectable()
export class EnrollmentConfirmationService {

    constructor(
        private readonly emailService: EmailService,
      ) {
    }

    /**
     * Send confirmation emails to primary contact email
     * on successful enrollment
     */
    async sendConfirmationEmail(contactEmail: string, travellers: Array<EnrollmentFormTravellerRO>): Promise<void> {
      try {
        let travellersString = [];
        travellers.map(element => travellersString.push(`${element.firstName} ${element.lastName}: ${element.confirmationNumber}`))

        const msg = new EnrollmentConfirmationEmail(
          contactEmail,
          {
            travellers: travellersString.join(', ')
          }
        );
        await this.emailService.sendEmail(msg);
      }
      catch (e) {
        LoggerService.error("Unable to send enrollment confirmation email.", e)
      }
    }
}



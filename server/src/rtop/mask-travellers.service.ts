import { EnrollmentFormSearchRO } from "./ro/enrollment-form-search.ro";
import { maxLength } from "class-validator";
import { LoggerService } from '../logs/logger';

interface Trie{
    [key: string]: TrieNode
}

class TrieNode {
    count: number;
    children: Trie;

    constructor()
    {
        this.count = this.count?? 1;
        this.children = this.children?? {};
    }

    incrementCount()
    {
        this.count += 1;
    }
}


export class MaskTravellersService {

    static maskString = "*";

    private static modifyString(data: string, count: number)
    {
        return data.substr(0, count) + this.maskString.repeat(data.length - count);
    }

    private static maskName(travellers: EnrollmentFormSearchRO[], property: string){
        let node: Trie = {};
        const root: Trie = node;
        travellers.forEach((traveller) => {
            node = root;
            [...traveller[property]].forEach((ele) => {
                if(node[ele])
                {
                    node[ele].incrementCount();
                }
                else{
                    node[ele] = new TrieNode();
                }
                node = node[ele].children;
            });
        });

        travellers.forEach((traveller) => {
            node = root;
            let matchedCount = 0;
            for (const ele of traveller[property]) {
                if(node[ele].count > 1 )
                {
                    node = node[ele].children;
                }
                else {
                    matchedCount++;
                    break;
                }
                matchedCount++;
            }
            const maxLength = (traveller[property].length < matchedCount) ? traveller[property].length : matchedCount;
            traveller[property] = this.modifyString(traveller[property], maxLength);
        });
    }
    
    static mask(travellers: EnrollmentFormSearchRO[]){
        try {
            this.maskName(travellers, "firstName");
            this.maskName(travellers, "lastName");    
        } catch (error) {
            LoggerService.error(error);
        }
    }
}
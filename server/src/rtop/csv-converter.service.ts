import { parse } from 'json2csv';
import { LoggerService } from '../logs/logger';
import { Injectable } from '@nestjs/common';

@Injectable()
export class CSVConverterService {

    convertJSONToCSV(fields: any, data: any) {
        // Map the name of the fields according to the fields dict 
        data = data.map(r => Object.entries(r).reduce((res, [key, value]) => {
            res[fields[key]] = value;
            return res;
        }, {}))

        try {
            const opts = { fields: Object.values(fields) };
            const csv = parse(data, opts);
            return csv;
        } catch (err) {
            LoggerService.error(err);
        }
    }
}
export class ReminderIDRO {
    id: number;
    travellerId: number;
    householdId: number;
    date: string;
    token: string;

    constructor(res, token) {
        // Convert uppercase properties (from db2 query) to properties
        this.id = res.ID;
        this.travellerId = res.TRAVELLER_ID;
        this.householdId = res.HOUSEHOLD_ID;
        this.date = res.DATE;
        this.token = token;
    }
}

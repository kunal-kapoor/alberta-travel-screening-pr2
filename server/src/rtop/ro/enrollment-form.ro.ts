import { RTOPTraveller } from "../entities/rtop-traveller.entity";
import { booleanToString, CitiesAndCountries } from "../enrollment-form.constants";
import { Household, EnrollmentStatus } from "../entities/household.entity";
import { HouseholdTravellersSubmissionStatusRO } from "./household-submission-status.ro";
import { QuarantineLocationRO } from "./quarantine-location.entity";

export class RTOPTravellerRO {
    firstName: string;
    lastName: string;
    confirmationNumber: string;
    dateOfBirth: Date;
    gender: string;
    citizenshipStatus: string;
    exemptionType: string;
    exemptOccupation: string;
    exemptOccupationDetails: string;
    dateLeavingCanada: string;
    seatNumber: string;
    reasonForTravel: string;
    durationOfStay: string;
    preDepartureTest: string;
    preDepartureTestDate: string;
    preDepartureTestCountry: string;
    preDepartureVaccination: string;
    timeSinceVaccination: string;
    dosesOfVaccine: string;
}

export class EnrollmentFormRO extends RTOPTravellerRO {
    id: number;
    viableIsolationPlan: boolean;
    phoneNumber: string;
    email: string;
    hasAdditionalTravellers: string;
    additionalTravellers: Array<RTOPTravellerRO>
    hasTravellersOutsideHousehold: string;
    travellersOutsideHousehold: string;
    nameOfAirportOrBorderCrossing: string;
    arrivalDate: string;
    arrivalCityOrTown: string;
    arrivalCountry: string;
    address: string;
    cityOrTown: string;
    provinceTerritory: string;
    postalCode: string;
    quarantineLocationPhoneNumber: string;
    quarantineLocation: QuarantineLocationRO;
    typeOfPlace: string;
    howToGetToPlace: string;
    enrollmentStatus: EnrollmentStatus;
    householdCardStatus: string;
    travellerStatus: HouseholdTravellersSubmissionStatusRO[];
    dailyReminderLink: string;
    created: Date;
    additionalCitiesAndCountries: Array<CitiesAndCountries>

    constructor(data: Household) {
        super();

        Object.assign(this, data);

        this.hasAdditionalTravellers = booleanToString(data.hasAdditionalTravellers);
        this.hasTravellersOutsideHousehold = booleanToString(data.hasTravellersOutsideHousehold);
        this.preDepartureTest = data.preDepartureTest != undefined ? booleanToString(data.preDepartureTest) : '';
        this.preDepartureVaccination = data.preDepartureVaccination != undefined ? booleanToString(data.preDepartureVaccination) : '';
        this.quarantineLocation = new QuarantineLocationRO();
        this.quarantineLocation.address = data.address;
        this.quarantineLocation.phoneNumber = data.quarantineLocationPhoneNumber;
        this.quarantineLocation.cityOrTown = data.cityOrTown;
        this.quarantineLocation.provinceTerritory = data.provinceTerritory;
        this.quarantineLocation.postalCode = data.postalCode;
        this.quarantineLocation.typeOfPlace = data.typeOfPlace;
        this.quarantineLocation.howToGetToPlace = data.howToGetToPlace;
        this.quarantineLocation.doesVulnerablePersonLiveThere = '';
        this.quarantineLocation.otherPeopleResiding = '';
        this.additionalTravellers = data.additionalTravellers && data.additionalTravellers.map((eachTraveller: RTOPTraveller) => {
            const res: RTOPTravellerRO = {
                ...eachTraveller,
                preDepartureTest: eachTraveller.preDepartureTest != undefined ? booleanToString(eachTraveller.preDepartureTest) : '',
                preDepartureVaccination: eachTraveller.preDepartureVaccination != undefined ? booleanToString(eachTraveller.preDepartureVaccination) : ''
            };
            return res;
        });
    }
}

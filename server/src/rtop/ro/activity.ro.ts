import { Exclude, Expose } from "class-transformer";
import moment from "moment-timezone";

/**
 * id
 * agent
 * status
 * note
 */
@Exclude()
export class RTOPActivityRO {

  @Expose()
  date: string;

  @Expose()
  agent: string;

  @Expose()
  note: string;

  @Expose()
  type: string;

  @Expose()
  status: string;

  constructor(activity: RTOPActivityRO) {
    activity.date = moment(activity.date).utc().format()
    Object.assign(this, activity);
  }
}

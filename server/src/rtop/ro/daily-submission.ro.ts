import { DailyStatusRO } from "./household-submission-status.ro";
import { ReminderIDRO } from "./reminder-id.ro";
import { USER_TIMEZONE, DATE_FORMAT } from "../enrollment-form.constants";
import moment from "moment";

class DailySubmissionRO extends DailyStatusRO{
    id: string;

    constructor(data: any) {
        const reminder = new ReminderIDRO(data, data['TOKEN']);
        super(data, reminder);
        this.id = data.id;
    }
}

export class DailySubmissionQueryRO{
    travellers: DailySubmissionRO[];

    constructor(data: any[]){
        this.travellers = data.map((ele) => {
            return new DailySubmissionRO({id:ele['CONFIRMATION_NUMBER'], date: ele['CREATED'], status: ele['STATUS'], submission: ele['SUBMISSION'], ...ele });
        });
    }


}
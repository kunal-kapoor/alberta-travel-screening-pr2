import { DailySubmissionAnswer } from "../enrollment-form.constants";
import { Exclude } from "class-transformer";
import { DAILY_STATUS } from "../constants";

export class DailySubmission {
    answers: DailySubmissionAnswer[];
}

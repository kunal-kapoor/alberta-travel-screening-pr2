import { Injectable } from "@nestjs/common";
import { query } from "../../db/db2connection";
import { enrollmentFormQuery, travellerStatusQuery } from "../../db/rtop-db2connectionUtils";
import { EnrollmentStatus } from "../entities/household.entity";
import { WITHDRAWN_REASON } from "../constants";


@Injectable()
// TODO: all the queries are for the arrival form -> kindly change them accordingly
export class TravellerQuery {

    private static readonly EditForm: string = `
        update household SET
            form_record=SYSTOOLS.JSON_UPDATE(form_record,?)
        where id=?`;

    private static readonly FindByTravellerConfirmationNumber: string = `
        select h.id, SYSTOOLS.BSON2JSON(h.form_record) as form_record, t.enrollment_status
        from household as h
        join rtop_traveller t on t.household_id=h.id
        WHERE LOWER(t.confirmation_number)=?`;

    private static readonly GetUpdateStatusCountPrefix = `
        select count(*) from FINAL table
        `;

    private static readonly GetUpdateStatusIdPrefix = `
        select ID from FINAL table
        `;

    private static readonly UpdateHouseholdWithdrawnStatus = `
        UPDATE HOUSEHOLD SET ENROLLMENT_STATUS = '${EnrollmentStatus.WITHDRAWN}' , 
            WITHDRAWN_DATE = CURRENT_TIMESTAMP, WITHDRAWN_REASON = ?
            WHERE ID = ? AND ENROLLMENT_STATUS != '${EnrollmentStatus.WITHDRAWN}'
        `;

    private static readonly UpdateHouseholdEnrolledStatus = `
    UPDATE HOUSEHOLD SET ENROLLMENT_STATUS = '${EnrollmentStatus.ENROLLED}' , 
        DETERMINATION_DATE = CURRENT_TIMESTAMP
        WHERE ID = ? AND ENROLLMENT_STATUS != '${EnrollmentStatus.ENROLLED}'
    `;

    private static readonly UpdateHouseholdEnrolledStatusAtDate = `
    UPDATE HOUSEHOLD SET ENROLLMENT_STATUS = '${EnrollmentStatus.ENROLLED}' , 
        DETERMINATION_DATE = ?
        WHERE ID = ?
    `;

    private static readonly AddHouseholdEnrollmentStatusHistory = `
        INSERT INTO ENROLLMENT_STATUS_HISTORY (HOUSEHOLD_ID, ENROLLMENT_STATUS, WITHDRAWN_REASON) VALUES (?, ?, ?);
        `;

    private static readonly UpdateTravellerWithdrawnStatus = `
        UPDATE RTOP_TRAVELLER SET ENROLLMENT_STATUS = '${EnrollmentStatus.WITHDRAWN}'  , 
            WITHDRAWN_DATE = CURRENT_TIMESTAMP, WITHDRAWN_REASON = ? 
            WHERE LOWER(CONFIRMATION_NUMBER) = ? AND ENROLLMENT_STATUS != '${EnrollmentStatus.WITHDRAWN}'
        `;

    private static readonly UpdateTravellerEnrolledStatus = `
        UPDATE RTOP_TRAVELLER SET ENROLLMENT_STATUS = '${EnrollmentStatus.ENROLLED}'  , 
            DETERMINATION_DATE = CURRENT_TIMESTAMP
            WHERE LOWER(CONFIRMATION_NUMBER) = ? AND ENROLLMENT_STATUS != '${EnrollmentStatus.ENROLLED}'
        `;

    private static readonly AddTravellerEnrollmentStatusHistory = `
        INSERT INTO TRAVELLER_ENROLLMENT_STATUS_HISTORY (TRAVELLER_ID, ENROLLMENT_STATUS, WITHDRAWN_REASON)
        `;

    private static readonly BackFillTravellerWithdrawnStatus = `
        UPDATE RTOP_TRAVELLER SET ENROLLMENT_STATUS = '${EnrollmentStatus.WITHDRAWN}'  , 
            WITHDRAWN_DATE = CURRENT_TIMESTAMP, WITHDRAWN_REASON = ? 
            WHERE HOUSEHOLD_ID = ? AND ENROLLMENT_STATUS != '${EnrollmentStatus.WITHDRAWN}'
        `;

    private static readonly BackFillTravellerEnrolledStatus = `
        UPDATE RTOP_TRAVELLER SET ENROLLMENT_STATUS = '${EnrollmentStatus.ENROLLED}'  , 
            DETERMINATION_DATE = CURRENT_TIMESTAMP
            WHERE HOUSEHOLD_ID = ? AND ENROLLMENT_STATUS = '${EnrollmentStatus.APPLIED}'
        `;

    private static readonly BackFillHouseholdWithdrawnStatus = `
        UPDATE HOUSEHOLD h SET 
            h.ENROLLMENT_STATUS = '${EnrollmentStatus.WITHDRAWN}', 
            WITHDRAWN_DATE = CURRENT_TIMESTAMP, WITHDRAWN_REASON = ?
            WHERE ID = ? AND NOT EXISTS (
                SELECT 1 FROM RTOP_TRAVELLER rtop 
                    WHERE rtop.HOUSEHOLD_ID = h.ID 
                        AND rtop.ENROLLMENT_STATUS <> '${EnrollmentStatus.WITHDRAWN}'
            )
        `;

    private static readonly getUpdateHouseholdQuery = (updatedFields) => `
    UPDATE HOUSEHOLD SET 
        form_record=SYSTOOLS.JSON_UPDATE(form_record,?)
        ${updatedFields}
        where ID = ?
    `;

    private static readonly getUpdateTravellerRecordQuery = (updatedFields) => `
    UPDATE RTOP_TRAVELLER SET 
        ${updatedFields}
        WHERE CONFIRMATION_NUMBER = ?;
    `;

    private static readonly getUpdateContactMethodQuery = (updatedFields) => `
    update contact_method_verification set
        ${updatedFields}
        WHERE HOUSEHOLD_ID = ?;
    `;


    private static readonly FindTravellersByToken = `
        SELECT
            rt.FIRST_NAME,
            rt.LAST_NAME,
            rt.CONFIRMATION_NUMBER,
            rt.ID,
            dr.DATE,
            ds.STATUS,
            h.ARRIVAL_DATE,
            h.DETERMINATION_DATE,
            rt.dob_attempt,
            rt.dob_attempt_time,
            h.CARD_STATUS_REASON,
            rt.BIRTH_DATE as DOB,
            h.ID as HOUSEHOLD_ID,
            TRIM(BOTH FROM JSON_VAL(h.form_record, 'nameOfAirportOrBorderCrossing', 's:200')) as AIRPORT_OR_BORDER_CROSSING,
            SYSTOOLS.BSON2JSON(ds.SUBMISSION) as SUBMISSION,
        CASE WHEN ds.id IS NULL THEN 'not_submitted' ELSE 'submitted' END AS submission_status
        FROM RTOP_TRAVELLER rt 
        INNER JOIN HOUSEHOLD h
        ON rt.household_id = h.ID
        INNER JOIN DAILY_REMINDER dr
        ON dr.household_id = h.ID
        LEFT JOIN daily_submission ds
        ON ds.REMINDER_ID = dr.id AND ds.TRAVELLER_ID  = rt.ID
        WHERE rt.ENROLLMENT_STATUS = '${EnrollmentStatus.ENROLLED}' and dr.TOKEN=?
    `;

    private static readonly UpdateLastScreened = `
        UPDATE rtop_traveller SET
            last_screened = ?
        WHERE id=?
    `;

    ////////////////////////////////////////

    /**
     * Updates last screened timestamp for the given traveller
     * @param travellerId ID of traveller to update
     */
    public static async updateLastScreened(travellerId: number, lastScreened: string) {
        await query(TravellerQuery.UpdateLastScreened, [lastScreened, travellerId]);
    }

    public static async updateHouseholdPrimaryTraveller(form_record: string, household: any, householdId: number) {
        const OBJECT_DB_NAME_MAP = {
            firstName: 'FIRST_NAME',
            lastName: 'LAST_NAME',
            email: 'EMAIL',
            phoneNumber: 'PHONE_NUMBER',
            contactEmail: 'CONTACT_EMAIL',
            contactPhoneNumber: 'CONTACT_PHONE_NUMBER'
        };

        if (Object.keys(household).length > 0) {
            const updatedFields = Object.keys(household).map((t) => `${OBJECT_DB_NAME_MAP[t]} = ?`);
            return query(TravellerQuery.getUpdateHouseholdQuery(`, ${updatedFields.join(" , ")}`),
                [form_record, ...Object.values(household), householdId]);
        }
        else {
            return query(TravellerQuery.getUpdateHouseholdQuery(""), [form_record, householdId]);
        }
    }

    public static async updateTravellerRecord(record: any, confirmationNumber: string) {
        const OBJECT_DB_NAME_MAP = {
            firstName: 'FIRST_NAME',
            lastName: 'LAST_NAME',
            dateOfBirth: 'BIRTH_DATE'
        };
        const updatedFields = Object.keys(record).map((t) => `${OBJECT_DB_NAME_MAP[t]} = ?`);
        const qs = TravellerQuery.getUpdateTravellerRecordQuery(updatedFields.join(" , "));
        return await query(qs, [...Object.values(record), confirmationNumber]);
    }

    public static async updateHouseholdContactMethod(record: any, householdId: number) {
        const OBJECT_DB_NAME_MAP = {
            email: 'EMAIL',
            phoneNumber: 'PHONE_NUMBER'
        };
        const updatedFields = Object.keys(record).map((t) => `${OBJECT_DB_NAME_MAP[t]} = ?`);
        const qs = TravellerQuery.getUpdateContactMethodQuery(updatedFields.join(" , "));
        return await query(qs, [...Object.values(record), householdId]);
    }

    public static async updateHouseholdAdditionalTravellers(updateFields: string, householdId: number) {
        return await query(TravellerQuery.EditForm, [updateFields, householdId])
    }

    public static async addHouseholdEnrollmentStatusHistory(householdId: number, enrollmentStatus: string, withdrawalReason: WITHDRAWN_REASON = null) {
        return query(TravellerQuery.AddHouseholdEnrollmentStatusHistory, [householdId, enrollmentStatus, withdrawalReason]);
    }

    /**
     * Get the household the traveller with the given confirmation is part of
     * 
     * @param confirmationNumber - confirmation number
     */
    public static async getFormByTravellerConfirmationNumber(confirmationNumber: string) {
        return await enrollmentFormQuery(TravellerQuery.FindByTravellerConfirmationNumber, [confirmationNumber.toLowerCase()]);
    }

    /**
     * Returns the count of number of rows updated by the query.
     * 
     * @param query update query string to executed
     */
    public static getUpdateCountQuery(query: string): string {
        return `${TravellerQuery.GetUpdateStatusCountPrefix} (${query})`;
    }

    /**
     * Returns the count of number of rows updated by the query.
     * 
     * @param query update query string to executed
     */
    public static getUpdatedIdQuery(query: string): string {
        return `${TravellerQuery.GetUpdateStatusIdPrefix} (${query})`;
    }

    public static async updateHouseholdWithdrawnStatus(id: number, withdrawalReason: WITHDRAWN_REASON) {
        return query(TravellerQuery.getUpdateCountQuery(TravellerQuery.UpdateHouseholdWithdrawnStatus), [withdrawalReason, id]);
    }

    public static async updateHouseholdEnrolledStatus(id: number, date?: string) {
        if(date) {
            return query(TravellerQuery.getUpdateCountQuery(TravellerQuery.UpdateHouseholdEnrolledStatusAtDate), [date, id]);
        } else {
            return query(TravellerQuery.getUpdateCountQuery(TravellerQuery.UpdateHouseholdEnrolledStatus), [id]);
        }
    }

    public static async updateTravellerWithdrawnStatus(travellerConfirmationNumber: string, withdrawalReason: WITHDRAWN_REASON) {
        return query(TravellerQuery.getUpdatedIdQuery(TravellerQuery.UpdateTravellerWithdrawnStatus),
            [withdrawalReason, travellerConfirmationNumber.toLowerCase()]);
    }

    public static async updateTravellerEnrolledStatus(travellerConfirmationNumber: string) {
        return query(TravellerQuery.getUpdatedIdQuery(TravellerQuery.UpdateTravellerEnrolledStatus),
            [travellerConfirmationNumber.toLowerCase()]);
    }

    public static async addTravellerEnrollmentStatusHistory(travellerIds: number[], enrollmentStatus: string, withdrawalReason: WITHDRAWN_REASON = null) {
        const insertQuery = `${TravellerQuery.AddTravellerEnrollmentStatusHistory} VALUES `;
        const placeholders = travellerIds.map(() => ` (?,?,?)`).join(",");
        const rows: any = travellerIds.map(r => [r, enrollmentStatus, withdrawalReason]);

        return await query(`${insertQuery} ${placeholders}`, rows.flat());
    }

    public static async backfillTravellerWithdrawnStatus(id: number, withdrawalReason: WITHDRAWN_REASON) {
        return query(TravellerQuery.getUpdatedIdQuery(TravellerQuery.BackFillTravellerWithdrawnStatus), [withdrawalReason, id]);
    }

    public static async backfillTravellerEnrolledStatus(id: number) {
        return query(TravellerQuery.getUpdatedIdQuery(TravellerQuery.BackFillTravellerEnrolledStatus), [id]);
    }

    public static async backfillHouseholdWithdrawnStatus(id: number, withdrawalReason: WITHDRAWN_REASON) {
        return query(TravellerQuery.getUpdateCountQuery(TravellerQuery.BackFillHouseholdWithdrawnStatus), [withdrawalReason, id]);
    }

    /**
     * Returns the list of travllers
     *
     * @param token - daily reminder id
     */
    public static async getFormByToken(token: string): Promise<any> {
        return await enrollmentFormQuery(TravellerQuery.FindTravellersByToken, [token]);
    }
}
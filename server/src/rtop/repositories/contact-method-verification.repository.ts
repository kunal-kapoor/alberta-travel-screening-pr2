import { Injectable } from "@nestjs/common";
import { RTOPDbService } from "../../db/rtop-dbservice";
import { ContactMethodVerification } from "../entities/contact-method-verification";
import { EnrollmentFormSubmissionRO } from "../ro/form-submission.ro";
import { ContactVerificationStatus } from "../entities/contact-method-verification-status";
import { CodeVerificationRO } from "../ro/code-verification.ro";

@Injectable()
export class ContactMethodVerificationRepository {
    /**
     * Verify a contact method base on code + phone number
     */
    async verifyCode(verificationId: string, code: string): Promise<CodeVerificationRO> {
        const record = await RTOPDbService.getVerificationTokenByCode(verificationId, code);

        if(record && record.value() && record.value().length) {
          return new CodeVerificationRO(record.value()[0])
        }

        return null;
    }

    /**
     * Mark the given contact method as verified, and assign it to the given household
     * @param verification contact method to verify
     * @param household household to assign the record to
     */
    async markAsVerified(verification: ContactMethodVerification, household: EnrollmentFormSubmissionRO) {
      await RTOPDbService.updateVerificationStatus(verification, household.id, ContactVerificationStatus.VERIFIED)
    }

    /**
     * Look up a verification by token
     * @param contactMethodToken tooken to look up
     */
    async findVerification(contactMethodToken: string): Promise<ContactMethodVerification> {
      const res = await RTOPDbService.getVerificationToken(contactMethodToken);
      
      if(res && res.value() && res.value().length) {
        return new ContactMethodVerification(res.value()[0]);
      }

      return null;
    }

}
import { Injectable } from "@nestjs/common";
import { EmailService, EmailSendable } from "../email/email.service";
import { RTopMonitoringRepository } from "./repositories/rtop-monitoring.repository";
import moment from "moment";
import { DATE_TIME_FORMAT, USER_TIMEZONE, DATE_FORMAT } from "./enrollment-form.constants";
import { EnrollmentStatus } from "./entities/household.entity";

class DailyStatsServiceEmail implements EmailSendable {
    from: string;
    to: string[];
    subject: string;
    template = 'daily-enrollment-stats';
    context: any;
    cc: string[];

    constructor(data: { enrollmentStats: any[], date: string }) {
        this.to = process.env.DAILY_STATS_RECIPIENTS.split(',');
        this.cc = process.env.DAILY_STATS_CC_RECIPIENTS && process.env.DAILY_STATS_CC_RECIPIENTS.split(',');
        this.from = process.env.DAILY_REMINDER_EMAIL_SENDER;
        this.context = data;
        this.subject = `Alberta Border Pilot: Enrollment Stats for ${data.date}`;
    }
}


@Injectable()
export class DailyStatsService {

    constructor(
        private readonly emailService: EmailService,
        private readonly monitoringPortalRepository: RTopMonitoringRepository,
    ) {
    }

    /**
     * Fetches the stats of the travellers enrolled for the previous day - airport wise.
     * 
     * Sends an email with the stats to the mentioned recipients
     * 
     */
    async dailyEnrollmentStats(): Promise<any> {
        const yesterday = moment.tz(USER_TIMEZONE).subtract('1', 'day');
        const fromDate = yesterday.startOf('day').utc().format(DATE_TIME_FORMAT);
        const toDate = yesterday.endOf('day').utc().format(DATE_TIME_FORMAT);
        //get enrolled travellers stats
        const res = await this.monitoringPortalRepository.generateTravellersReport(fromDate, toDate, EnrollmentStatus.ENROLLED);
        this.sendEnrollmentStatsEmail(res, yesterday.format(DATE_FORMAT));
    }

    async sendEnrollmentStatsEmail(res: any[], date: string): Promise<void> {
        const msg = new DailyStatsServiceEmail(
            {
                enrollmentStats: res,
                date
            }
        );
        await this.emailService.sendEmail(msg);
    }
}



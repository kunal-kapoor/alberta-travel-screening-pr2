import { Injectable } from "@nestjs/common";
import { randomBytes } from "crypto";
import { EnrollmentFormRepository } from "./repositories/enrollment-form.repository";
import { LoggerService } from "../logs/logger";

const MAX_TRIES = 500;
export class ValidationNumberGenerationError extends Error {
  constructor(message: string) {
      super(message);

      Object.setPrototypeOf(this, ValidationNumberGenerationError.prototype);
  }
}

/**
 * Utility to generate a unique random confirmation code for an ArrivalForm.
 */
@Injectable()
export class EnrollmentFormValidationNumberGenerator {
  maxTries = MAX_TRIES;
  enrollmentFormRepository: EnrollmentFormRepository;

  constructor(enrollmentFormRepository: EnrollmentFormRepository) {
    this.enrollmentFormRepository = enrollmentFormRepository;
  }

  generate(): string {
    return 'AB' + randomBytes(3).toString('hex').toUpperCase().slice(0, -1);
  }
  /**
   * Generates a unique random validation code for the given entity.
   * - Checks the repository of the given entity to make sure the code is not already used.
   */
  async generateValidationNumber(): Promise<string> {
    let tries = 0;
    // Make sure we don't end up in an infinite loop in the unlikely event that we fail to generate a unique code.
    while (tries < this.maxTries) {
      const code = this.generate();
      const exists = await this.enrollmentFormRepository.countByConfirmationNumber(code) > 0;

      if (!exists) {
        return code;
      }

      tries++;
    }
    // Fail hard if we failed to generate a validation number
    LoggerService.error('Failed to generate validation number, number of tries exceeded max');
    throw new ValidationNumberGenerationError('Failed to generate validation number, number of tries exceeded max');

  }
}

import { RTOPCronController } from "./rtop-admin/rtop-cron.controller";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { SlackNotifierService } from "./slack-notifier.service";
import moment from "moment";
import { DATE_TIME_FORMAT } from "./rtop/enrollment-form.constants";


(async () => {
    const appContext = await NestFactory.createApplicationContext(AppModule);
    const rtopController = await appContext.get(RTOPCronController);
    const slackNotifierService = await appContext.get(SlackNotifierService);
    try {
        await slackNotifierService.postMessage(`${process.env.CRON_JOB} task started at ${moment().format(DATE_TIME_FORMAT)} UTC time.`);
        switch (process.env.CRON_JOB) {
            case "DAILY_YELLOW_CHECK":
                rtopController.runDailyCheck();
                break;
            case "DAILY_CHECKIN":
                rtopController.runJob();
                break;
            case "DAILY_REMINDER":
                rtopController.runDailyCheckReminder();
                break;
            case "DAILY_STATS":
                rtopController.runDailyStats();
                break;
            default:
                break;
        }
    } catch (error) {
        await slackNotifierService.postMessage(`${process.env.CRON_JOB} task failed, due to ${error}.`);
    }

})();

module.exports = {};
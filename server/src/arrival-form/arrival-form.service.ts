import moment from 'moment';
import { Injectable } from "@nestjs/common";
import { LoggerService } from '../logs/logger';
import { IsolationPlanReportRepository, IsolationPlanReportProperty } from './repositories/isolation-plan.repository';
import { ArrivalFormRepository } from './repositories/arrival-form.repository';
import { CitiesAndCountries, AdditionalTravellers } from './arrival-form.constants';
import { ArrivalForm } from './entities/arrival-form.entity';
import { AdditionalCityAndCountry } from './entities/additional-city-and-country.entity';
import { AdditionalTraveller } from './entities/additional-traveller.entity';
import { ArrivalFormValidationNumberGenerator } from './validation-number.service';
import { SubmissionDeterminationDTO } from './dto/submission-determination.dto';
import { ArrivalFormDTO } from './dto/arrival-form.dto';
import { ArrivalFormSubmissionRO } from './ro/form-submission.ro';
import { ArrivalFormRO } from './ro/arrival-form.ro';
import { EditFormDTO } from './dto/edit-form.dto';
import { AgentDTO } from '../service-alberta/service-alberta.repository';

export enum DeterminationDecision {
    ACCEPTED = 'Accepted'
}

const isViableIsolationPlan = (dto: ArrivalFormDTO): boolean => {
    return dto.hasPlaceToStayForQuarantine
    && dto.quarantineLocation && !dto.quarantineLocation.doesVulnerablePersonLiveThere
    && dto.isAbleToMakeNecessaryArrangements
};

@Injectable()
export class ArrivalFormService {
    constructor(
        private readonly arrivalFormRepository: ArrivalFormRepository,
        private readonly isolationPlanReportRepository: IsolationPlanReportRepository,
        private readonly confirmationNumberGenerator:ArrivalFormValidationNumberGenerator
      ) {}

    /**
     * Creates a new ArrivalForm
     * - Generate a unique confirmation number
     * - Create entity
     * - Update daily report based on isolation plan status
     *
     * @param body data of the arrival form to create.
     */
    async saveForm(body: ArrivalFormDTO): Promise<ArrivalFormSubmissionRO> {
        const confirmationNumber = await this.confirmationNumberGenerator.generateValidationNumber()
        const form: ArrivalForm = this.mapArrivalFormDtoToEntity(body,confirmationNumber);

        await this.arrivalFormRepository.save(form);
        await this.createOrUpdateReport(moment(form.created).format('YYYY/MM/DD'), form.viableIsolationPlan);
        return new ArrivalFormSubmissionRO(
            {
                id: form.id,
                message: 'Form submission successful',
                confirmationNumber,
                viableIsolationPlan: form.viableIsolationPlan,
            }
        );
    }

    /**
     * Mapping from ArrivalFormDTO + Confirmation number -> ArrivalForm
     * @param dto
     * @param confirmationNumber
     */
    mapArrivalFormDtoToEntity(dto: ArrivalFormDTO, confirmationNumber: string): ArrivalForm {
        return <ArrivalForm> {
            ...dto,
            id: null,
            confirmationNumber,
            viableIsolationPlan: isViableIsolationPlan(dto),
            determination: undefined,
            created: new Date(),
            dateOfBirth: moment(dto.dateOfBirth,'YYYY/MM/DD').toDate(),
            arrivalDate: moment(dto.arrivalDate,'YYYY/MM/DD').toDate(),
            additionalTravellers: dto.additionalTravellers && dto.additionalTravellers.map(this.mapToAdditionalTraveller),
            additionalCitiesAndCountries: dto.additionalCitiesAndCountries && dto.additionalCitiesAndCountries.map(this.mapToAdditionalCountry),
        }
    }

    mapToAdditionalTraveller(additionalTraveller: AdditionalTravellers): AdditionalTraveller {
        return {
            firstName: additionalTraveller.firstName,
            lastName: additionalTraveller.lastName,
            dateOfBirth: moment(additionalTraveller.dateOfBirth,'YYYY/MM/DD').toDate(),
        }
    }

    mapToAdditionalCountry(additionalLocation: CitiesAndCountries): AdditionalCityAndCountry {
        return {
            cityOrTown: additionalLocation.cityOrTown,
            country: additionalLocation.country,
        }
    }

    /**
     * Submits determination for the given arrivalForm
     * - Updates the report based on determination
     * @param arrivalForm the form to associate determination with
     * @param determination the determination to submit
     */
    async submitDetermination(arrivalForm: ArrivalFormRO, determination: SubmissionDeterminationDTO): Promise<void> {
        const submittedDetermination = await this.arrivalFormRepository.submitDetermination(arrivalForm.id, determination);
        await this.updateDeterminationReport(arrivalForm, determination.determination === DeterminationDecision.ACCEPTED);
        return submittedDetermination;
    }

    /**
     * Updates the given arrival form with new values
     * @param arrivalForm the form to update
     * @param updatedFields the new values to update the form with
     * 
     * @returns string[] - Array of the name of the fields that were updated
     */
    async editForm(agent: AgentDTO, arrivalForm: ArrivalFormRO, updatedFields: EditFormDTO): Promise<string[]> {
        // Update form
        await this.arrivalFormRepository.editForm(agent.id, arrivalForm, updatedFields);

        // Find the name of quarantine location fields that were actually updated
        const updatedForm: ArrivalFormRO = await this.arrivalFormRepository.findOne(arrivalForm.id);

        const updatedFieldNames = Object.keys(updatedFields.quarantineLocation || {})
            .filter(field => (updatedForm.quarantineLocation || {})[field] !== (arrivalForm.quarantineLocation || {})[field]);

        // Add hasPlaceToStayForQuarantine to the list of updated field names if it was updated
        if(updatedForm.hasPlaceToStayForQuarantine !== arrivalForm.hasPlaceToStayForQuarantine) {
            updatedFieldNames.push('hasPlaceToStayForQuarantine');
        }
        
        return updatedFieldNames;
        
    }

    /**
     * Creates or updates the daily report based on whether the form passed isolation check or not
     */
    async createOrUpdateReport(date: string, passedIsolationCheck: boolean): Promise<void> {
        const toIncrement = passedIsolationCheck? IsolationPlanReportProperty.PASSED: IsolationPlanReportProperty.FAILED;
        await this.incrementForDate(date, toIncrement);
    }

    /**
     * Updates the determination reports failedThenPassed/passedThenFailed based on the given isolation status
     */
    async updateDeterminationReport(arrivalForm, isolationStatus:boolean): Promise<void> {
        // If the new value is the same as the old one, do nothing
        if (isolationStatus === arrivalForm.viableIsolationPlan) {
            return;
        }

        const date = moment(arrivalForm.created).format('YYYY/MM/DD');
        const toIncrement = isolationStatus? IsolationPlanReportProperty.FAILED_THEN_PASSED: IsolationPlanReportProperty.PASSED_THEN_FAILED;
        await this.incrementForDate(date, toIncrement);
    }

    /**
     * Increments the current dates report for the current property
     */
    async incrementForDate(date: string, property: IsolationPlanReportProperty): Promise<void> {
        try {
            const currentReport = await this.isolationPlanReportRepository.findByDate(date);
            if (currentReport) {
                await this.isolationPlanReportRepository.increment(currentReport.id, property);
            } else {
                await this.isolationPlanReportRepository.createForDate(date, property);
            }
        } catch (e) {
            LoggerService.error('Failed to increment for date', e);
            throw e;
        }
    }
}

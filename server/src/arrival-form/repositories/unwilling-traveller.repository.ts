import { Injectable } from "@nestjs/common";
import { UnwillingTravellerDTO } from "../dto/unwilling-traveller.dto";
import { DbService } from "../../db/dbservice";
import { UnwillingTravellerRO } from "../ro/unwilling-traveller.ro";

@Injectable()
export class UnwillingTravellerRepository {

    /**
     *
     * Adds a new unwilling traveller record
     *
     * @param agent ID of the agent that performed the action
     * @param unwillingTraveller The traveller record to add
     *
     * @returns The created record
     */
    async recordUnwillingTraveller(agent: string, unwillingTraveller: UnwillingTravellerDTO): Promise<UnwillingTravellerRO> {
        return await DbService.recordUnwillingTraveller(agent, unwillingTraveller.numTravellers, unwillingTraveller.note);
    }
    /**
     *
     * Retrieves an unwilling traveller record
     *
     * @param id ID of the record to retrieve
     *
     * @returns The record
     */
    async getUnwillingTraveller(id: number): Promise<UnwillingTravellerRO> {
        return await DbService.getUnwillingTraveller(id);
    }
}
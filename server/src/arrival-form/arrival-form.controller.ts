import { Controller, Body, Post, UseInterceptors, ClassSerializerInterceptor, Req } from '@nestjs/common';
import { ArrivalFormDTO } from './dto/arrival-form.dto';
import { ArrivalFormService } from './arrival-form.service';
import { PublicRoute } from '../decorators';
import { LoggerService } from '../logs/logger';
import { ActionName } from '../logs/enums';
import { ArrivalFormSubmissionRO } from './ro/form-submission.ro';


@Controller('api/v1/form')
@UseInterceptors(ClassSerializerInterceptor)
export class ArrivalFormController {
    constructor(
        private readonly arrivalFormService:ArrivalFormService
        ){}

    @PublicRoute()
    @UseInterceptors(ClassSerializerInterceptor)
    @Post()
    async submitArrivalForm(@Req() req, @Body() body:ArrivalFormDTO):Promise<ArrivalFormSubmissionRO>{
        const form = await this.arrivalFormService.saveForm(body);

        LoggerService.logData(req, ActionName.CREATE, null, [form.id]);

        return form;
    }
}

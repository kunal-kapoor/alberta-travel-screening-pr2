import { Exclude } from "class-transformer";

export class ArrivalFormSubmissionRO {
    @Exclude()
    id: number;
    message: string;
    confirmationNumber: string;
    viableIsolationPlan: boolean;

    constructor(data: Partial<ArrivalFormSubmissionRO>) {
        Object.assign(this, data);
    }
}

import { ArrivalForm } from "../entities/arrival-form.entity";
import { IsArray, ValidateNested } from "class-validator";
import { Exclude, Expose } from "class-transformer";

@Exclude()
export class ArrivalFormSearchRO {
    @Expose()
    id: number;

    @Expose()
    confirmationNumber: string;

    @Expose()
    firstName: string;

    @Expose()
    lastName: string;

    @Expose()
    arrivalCityOrTown: string;

    @Expose()
    arrivalCountry: string;

    @Expose()
    arrivalDate: Date;

    constructor(data: ArrivalForm) {
        Object.assign(this, data);
    }
}

export class ArrivalFormSearchResultRO {
    @IsArray()
    @ValidateNested({each: true})
    travellers: ArrivalFormSearchRO[];

    constructor(trls: ArrivalForm[]) {
        this.travellers = trls.map(t => new ArrivalFormSearchRO(t));
    }
}

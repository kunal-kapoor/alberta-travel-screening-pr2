import { chunks } from "../../utils/chunks"


describe('chunks', () => {
    it('should return empty array when input is empty', () => {
        const res = chunks([], 5);

        expect(res).toEqual([]);
    });

    it('should split into chunks of one when 1 is specified', () => {
        const res = chunks(['abc', '123', '456'], 1);

        expect(res).toEqual([['abc'], ['123'], ['456']]);
    });

    it('should split into chunks of correct size when number is even', () => {
        const res = chunks(['abc', '123', '456', '789'], 2);

        expect(res).toEqual([['abc', '123'], ['456', '789']]);
    });

    it('should include leftover chunks when cannot fill completely', () => {
        const res = chunks(['abc', '123', '456', '789'], 3);

        expect(res).toEqual([['abc', '123', '456'], ['789']]);
    });

    it('should include all when number matches array size', () => {
        const res = chunks(['abc', '123', '456', '789'], 4);

        expect(res).toEqual([['abc', '123', '456', '789']]);
    });

    it('should include all when number is greater than array size', () => {
        const res = chunks(['abc', '123', '456', '789'], 7);

        expect(res).toEqual([['abc', '123', '456', '789']]);
    });

    it('should include all when number is negative', () => {
        const res = chunks(['abc', '123', '456', '789'], -2);

        expect(res).toEqual([['abc', '123', '456', '789']]);
    });

    it('should include all when number is 0', () => {
        const res = chunks(['abc', '123', '456', '789'], 0);

        expect(res).toEqual([['abc', '123', '456', '789']]);
    });
});
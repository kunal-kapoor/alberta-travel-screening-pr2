import { createModule } from "../util/db-test-utils";
import { ArrivalForm } from "../../arrival-form/entities/arrival-form.entity";
import { ArrivalFormRepository } from "../../arrival-form/repositories/arrival-form.repository";
import { envCond } from "../util/test-utils";

describe('findByLastName', () => {
  let repository: ArrivalFormRepository;
  let module;

  beforeEach(async () => {
    module = await createModule({ entities: [ArrivalForm, ArrivalFormRepository], providers: [ArrivalFormRepository], imports: [] });
    repository = module.module.get(ArrivalFormRepository);
  });

  afterEach(async () => {
    await module.close();
  });

  const seed = async () => {
    await (await module.seedBuilder(3))
      .transform(([el0, el1, el2]) => {
        el0.lastName = 'Ray';
        el2.lastName = 'Rayland';
        el1.lastName = 'Johnson';

        return [el0, el1, el2];
      })
      .create();
  }

  describe('lastName matches one result exactly', () => {
    let res;

    beforeEach(async () => {
      await seed();
      res = await repository.findByLastName('Rayland');
    });

    envCond('TRAVELLER')('returns result', async () => {
      expect(res.travellers[0].lastName).toBe('Rayland');
    });

    envCond('TRAVELLER')('returns one result', async () => {
      expect(res.travellers.length).toBe(1);
    });
  });

  describe('lastName matches two results exactly', () => {
    let res;

    beforeEach(async () => {
        await (await module.seedBuilder(3))
          .transform(([el0, el1, el2]) => {
            el0.lastName = 'Ray';
            el2.lastName = 'Johnson';
            el2.firstName = 'Ben';
            el1.lastName = 'Johnson';
            el1.firstName = 'John';
    
            return [el0, el1, el2];
          })
          .create();
    
          res = await repository.findByLastName('Johnson');
    });

    envCond('TRAVELLER')('returns result', async () => {
      expect(res).toEqual(
        expect.objectContaining({
          travellers: expect.arrayContaining([
            expect.objectContaining({ lastName: 'Johnson', firstName: 'Ben' }),
            expect.objectContaining({ lastName: 'Johnson', firstName: 'John' })
          ]),
        }),
      );
    });

    envCond('TRAVELLER')('returns two result', async () => {
      expect(res.travellers.length).toBe(2);
    });
  });

  describe('lastName matches two results partially', () => {
    let res;

    beforeEach(async () => {
      await seed();
      res = await repository.findByLastName('ray');
    });

    envCond('TRAVELLER')('returns result', async () => {
      expect(res).toEqual(
        expect.objectContaining({
          travellers: expect.arrayContaining([
            expect.objectContaining({ lastName: 'Ray' }),
            expect.objectContaining({ lastName: 'Rayland' })
          ]),
        }),
      );
    });

    envCond('TRAVELLER')('returns two result', async () => {
      expect(res.travellers.length).toBe(2);
    });
  });

  describe('lastName matches end of entries', () => {
    let res;

    beforeEach(async () => {
      await seed();
      res = await repository.findByLastName('son');
    });

    envCond('TRAVELLER')('returns no results', async () => {
      expect(res.travellers.length).toBe(0);
    });
  });

  describe('lastName is empty', () => {
    let res;

    beforeEach(async () => {
      await seed();
      res = await repository.findByLastName('');
    });

    envCond('TRAVELLER')('returns no results', async () => {
      expect(res.travellers.length).toBe(0);
    });
  });

});

import { createModule } from "../util/db-test-utils";
import { ArrivalForm } from "../../arrival-form/entities/arrival-form.entity";
import { ArrivalFormValidationNumberGenerator , ValidationNumberGenerationError } from "../../arrival-form/validation-number.service";
import { ArrivalFormRepository } from "../../arrival-form/repositories/arrival-form.repository";
import { envCond } from "../util/test-utils";

describe('generateValidationNumber', () => {
  let generator: ArrivalFormValidationNumberGenerator;
  let module;
  const CONFIRMATION_NUMBER = 'THISISARANDOMCODE';
  let generateFunc;
  
  beforeEach(async () => {
    module = await createModule({ entities: [ArrivalForm], providers: [ArrivalFormValidationNumberGenerator, ArrivalFormRepository], imports: []});
    generator = module.module.get(ArrivalFormValidationNumberGenerator);
    generateFunc = jest.fn();
    // Note: Mocking the random string generator so we can test things easier.
    generator.generate = generateFunc;
  });

  afterEach(async () => {
    await module.close();
  });

  describe('unique code is generated on first try', () => {
    let res;

    beforeEach(async () => {
      await (await module.seedBuilder(3))
        .transform(([el0, el1, el2]) => {
          el0.confirmationNumber = 'AB53kmn';
          el2.confirmationNumber = 'AB13WER';
          el1.confirmationNumber = 'ABRKN34';
          return [el0, el1, el2];
        })
        .create();


      generateFunc.mockReturnValueOnce(CONFIRMATION_NUMBER);

      res = await generator.generateValidationNumber();
    })

    envCond('ALL')('returns code', async () => {
      expect(res).toBe(CONFIRMATION_NUMBER);
    });
  });

  describe('unique code is generated on third try', () => {
    let res;
    const CONFLICTING_NUMBER = 'THIS_CODE_CONFLICTS';

    beforeEach(async () => {
      await (await module.seedBuilder(3))
        .transform(([el0, el1, el2]) => {
          el0.confirmationNumber = 'AB53kmn';
          el2.confirmationNumber = 'AB13WER';
          el1.confirmationNumber = CONFLICTING_NUMBER;

          return [el0, el1, el2];
        })
        .create();

        generateFunc
            .mockReturnValueOnce(CONFLICTING_NUMBER)
            .mockReturnValueOnce(CONFLICTING_NUMBER)
            .mockReturnValueOnce(CONFIRMATION_NUMBER);

      res = await generator.generateValidationNumber();
    })

    envCond('ALL')('returns code', async () => {
      expect(res).toBe(CONFIRMATION_NUMBER);
    });
  });

  describe('unique code fails to generate', () => {
    const CONFLICTING_NUMBER = 'THIS_CODE_CONFLICTS';
    const maxTries = 10;

    beforeEach(async () => {
      await (await module.seedBuilder(3))
        .transform(([el0, el1, el2]) => {
          el0.confirmationNumber = 'AB53kmn';
          el2.confirmationNumber = 'AB13WER';
          el1.confirmationNumber = CONFLICTING_NUMBER;

          return [el0, el1, el2];
        })
        .create();

        generator.maxTries = maxTries;

        Array(maxTries).fill(0).forEach(() => generateFunc.mockReturnValueOnce(CONFLICTING_NUMBER));
    })

    envCond('ALL')('throws exception', async () => {
      try {
        await generator.generateValidationNumber();
        fail('Did not throw exception');
      } catch(e){
        expect(e).toBeInstanceOf(ValidationNumberGenerationError)
      }
    });
  });

  describe('unique code is generated on second to last try', () => {
    let res;
    const CONFLICTING_NUMBER = 'THIS_CODE_CONFLICTS';
    const maxTries = 10;

    beforeEach(async () => {
      await (await module.seedBuilder(3))
        .transform(([el0, el1, el2]) => {
          el0.confirmationNumber = 'AB53kmn';
          el2.confirmationNumber = 'AB13WER';
          el1.confirmationNumber = CONFLICTING_NUMBER;

          return [el0, el1, el2];
        })
        .create();
      
        generator.maxTries = maxTries

      Array(maxTries - 2).fill(0).forEach(() =>generateFunc.mockReturnValueOnce(CONFLICTING_NUMBER));
      generateFunc.mockReturnValueOnce(CONFIRMATION_NUMBER);

      res = await generator.generateValidationNumber();
    })

    envCond('ALL')('returns code', async () => {
      expect(res).toBe(CONFIRMATION_NUMBER);
    });
  });

});

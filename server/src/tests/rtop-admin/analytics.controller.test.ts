import request from 'supertest';
import { createTestingApp } from '../util/db-test-utils';
import { envCond } from "../util/test-utils";
import { ConfigModule } from '@nestjs/config';
import { ClearRTOPDB } from '../../db/db2rtopmigrations';
import { createStaticVerification, createStaticEnrollmentFormWithoutAdditionalTravellers, createStaticEnrollmentFormWithAdditionalTravellers } from '../util/rtop-data-generator';
import { DATE_FORMAT, USER_TIMEZONE } from '../../rtop/enrollment-form.constants';
import moment from 'moment';
import { WITHDRAWN_REASON } from '../../rtop/constants';

const getConfirmationNumbers = async (app, withAdditionalTravellers = false) => {
    const verification = createStaticVerification();
    const res = await request(app.server())
        .post('/api/v2/rtop/traveller/verification')
        .send(verification)
        .expect(201);

    expect(res.body.token).toBeDefined();
    const verificationToken = res.body.token;
    const tempForm = withAdditionalTravellers ?
        createStaticEnrollmentFormWithAdditionalTravellers() :
        createStaticEnrollmentFormWithoutAdditionalTravellers();

    const enrollmentRes = await request(app.server())
        .post(`/api/v2/rtop/traveller/enroll/${verificationToken}`)
        .send(tempForm)
        .expect(201);

    expect(enrollmentRes.body.travellers).toBeDefined();

    return enrollmentRes.body.travellers.map(e => e.confirmationNumber);
}

const enrollTravellers = async (app, confirmationNumber) => {
    await request(app.server())
        .patch(`/api/v2/rtop-admin/form/${confirmationNumber.split("-")[0]}/enroll`)
        .expect(200);
}

const withdrawHousehold = async (app, confirmationNumber) => {
    await request(app.server())
        .patch(`/api/v2/rtop-admin/form/${confirmationNumber.split("-")[0]}/withdraw`)
        .send({withdrawnReason: WITHDRAWN_REASON.OTHER})
        .expect(200);
}

const enrollTraveller = async (app, confirmationNumber) => {
    await request(app.server())
        .patch(`/api/v2/rtop-admin/traveller/${confirmationNumber}/enroll`)
        .expect(200);
}

const withdrawTraveller = async (app, confirmationNumber) => {
    await request(app.server())
        .patch(`/api/v2/rtop-admin/traveller/${confirmationNumber}/withdraw`)
        .send({withdrawnReason: WITHDRAWN_REASON.OTHER})
        .expect(200);
}

const staticAPIResponse = (confirmationNumber, created, enrollmentDate, enrollmentStatus, withdrawnDate) => {
    const res = {
        travellers: [
            {
                dateOfBirth: '1954-01-31',
                gender: 'Male',
                citizenshipStatus: 'Citizen',
                exemptionType: 'Exempt',
                hasAdditionalTravellers: 'Yes',
                hasTravellersOutsideHousehold: 'No',
                nameOfAirportOrBorderCrossing: 'Airport - Calgary International (YYC)',
                arrivalDate: '2020-11-26',
                arrivalCityOrTown: 'Cotton',
                arrivalCountry: 'Cambridgeshire',
                dateLeftCanada: '2020-09-08',
                countryOfResidence: 'Cambridgeshire',
                airline: 'multi-byte niches focus group Cheese withdrawal payment Usability drive Books Manitoba',
                seatNumber: 'rgt',
                durationOfStay: '346',
                dateLeavingCanada: '2020-12-10',
                address: '77191 Walsh Brooks',
                provinceTerritory: 'Northwest Territories',
                postalCode: 'D0D 6E5',
                typeOfPlace: 'generating',
                howToGetToPlace: 'olive',
                enrollmentStatus,
                viableIsolationPlan: true,
                created,
                id: `${confirmationNumber}-0`,
                enrollmentDate,
                travellerStatus: null,
                lastUpdated: null,
                withdrawnDate,
                firstName: 'Jenifer',
                lastName: 'Lynch',
                preDepartureVaccination: 'No',
                preDepartureTest: 'No'
            },
            {
                dateOfBirth: '1967-10-12',
                gender: 'Male',
                citizenshipStatus: 'Citizen',
                exemptionType: 'Exempt',
                hasAdditionalTravellers: 'Yes',
                hasTravellersOutsideHousehold: 'No',
                nameOfAirportOrBorderCrossing: 'Airport - Calgary International (YYC)',
                arrivalDate: '2020-11-26',
                arrivalCityOrTown: 'Cotton',
                arrivalCountry: 'Cambridgeshire',
                dateLeftCanada: '2020-09-08',
                countryOfResidence: 'Cambridgeshire',
                airline: 'multi-byte niches focus group Cheese withdrawal payment Usability drive Books Manitoba',
                seatNumber: 'rgt',
                durationOfStay: '346',
                dateLeavingCanada: '2020-12-10',
                address: '77191 Walsh Brooks',
                provinceTerritory: 'Northwest Territories',
                postalCode: 'D0D 6E5',
                typeOfPlace: 'generating',
                howToGetToPlace: 'olive',
                enrollmentStatus,
                viableIsolationPlan: true,
                created,
                id: `${confirmationNumber}-1`,
                enrollmentDate,
                travellerStatus: null,
                lastUpdated: null,
                withdrawnDate,
                firstName: 'Leonardo',
                lastName: 'Caprio',
                preDepartureVaccination: 'No',
                preDepartureTest: 'No'
            }
        ]
    };

    return res;
}

const verifyAPIStructure = async (apiResponse: any, enrollmentStatus = 'enrolled', hasAdditionalTravellers = false) => {
    const expectedAPIResponse = staticAPIResponse(apiResponse.travellers[0].id.split("-")[0],
        apiResponse.travellers[0].created, apiResponse.travellers[0].enrollmentDate, enrollmentStatus, apiResponse.travellers[0].withdrawnDate);
    if(!hasAdditionalTravellers)
    {
        expectedAPIResponse.travellers.pop();
    }
    expect(apiResponse).toEqual(expectedAPIResponse);
}

const verifyEmptyAPIResponse = async (apiResponse: any) => {
    expect(apiResponse).toEqual({ travellers: [] });
}

describe('Analytics API retrieve enrolled travellers - only primary traveller', () => {
    let app;
    const URL = '/api/v2/rtop/analytics/enrolled';
    let API_HEADER;

    beforeAll(async () => {
        app = await createTestingApp({ imports: [ConfigModule.forRoot()], entities: null, providers: null }, { disableAuth: true });
        await app.start();
        await ClearRTOPDB();
        API_HEADER = process.env.AHS_TEST_HEADER;
    });

    afterAll(async () => {
        await ClearRTOPDB();
        await app.close();
    });

    envCond('ADMIN')('enrolls traveller', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        const res = await request(app.server())
            .get(URL)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyEmptyAPIResponse(apiResponse);

        await enrollTraveller(app, confirmationNumbers[0]);
    });

    envCond('ADMIN')('returns enrolled travellers with no date', async () => {
        const res = await request(app.server())
            .get(URL)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse);
    });

    envCond('ADMIN')('returns enrolled travellers with todays date', async () => {
        const res = await request(app.server())
            .get(URL)
            .query(`date=${moment().tz(USER_TIMEZONE).format(DATE_FORMAT)}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse);
    });

    envCond('ADMIN')('returns enrolled travellers with future date', async () => {
        const res = await request(app.server())
            .get(URL)
            .query(`date=${moment().tz(USER_TIMEZONE).add(1, 'day').format(DATE_FORMAT)}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyEmptyAPIResponse(apiResponse);
    });

    envCond('ADMIN')('returns enrolled travellers with previous date', async () => {
        const res = await request(app.server())
            .get(URL)
            .query(`date=${moment().tz(USER_TIMEZONE).subtract('1','day').format(DATE_FORMAT)}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyEmptyAPIResponse(apiResponse);
    });
});

describe('Analytics API retrieve enrolled travellers - additional traveller', () => {
    let app;
    const URL = '/api/v2/rtop/analytics/enrolled';
    let API_HEADER;

    beforeAll(async () => {
        app = await createTestingApp({ imports: [ConfigModule.forRoot()], entities: null, providers: null }, { disableAuth: true });
        await app.start();
        await ClearRTOPDB();
        API_HEADER = process.env.AHS_TEST_HEADER;
    });

    afterAll(async () => {
        await ClearRTOPDB();
        await app.close();
    });

    envCond('ADMIN')('enrolls travellers', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        const res = await request(app.server())
            .get(URL)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyEmptyAPIResponse(apiResponse);

        await enrollTraveller(app, confirmationNumbers[0]);
    });

    envCond('ADMIN')('returns enrolled travellers with no date', async () => {
        const res = await request(app.server())
            .get(URL)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, 'enrolled', true);
    });

    envCond('ADMIN')('returns enrolled travellers with todays date', async () => {
        const res = await request(app.server())
            .get(URL)
            .query(`date=${moment().tz(USER_TIMEZONE).format(DATE_FORMAT)}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, 'enrolled', true);
    });

    envCond('ADMIN')('returns enrolled travellers with future date', async () => {
        const res = await request(app.server())
            .get(URL)
            .query(`date=${moment().tz(USER_TIMEZONE).add(1, 'day').format(DATE_FORMAT)}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyEmptyAPIResponse(apiResponse);
    });
});


describe('Analytics API retrieve withdrawn travellers - only primary traveller', () => {
    let app;
    const URL = '/api/v2/rtop/analytics/withdrawn';
    let API_HEADER;

    beforeAll(async () => {
        app = await createTestingApp({ imports: [ConfigModule.forRoot()], entities: null, providers: null }, { disableAuth: true });
        await app.start();
        await ClearRTOPDB();
        API_HEADER = process.env.AHS_TEST_HEADER;
    });

    afterAll(async () => {
        await ClearRTOPDB();
        await app.close();
    });

    envCond('ADMIN')('withdraws travellers', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        {
            const res = await request(app.server())
                .get(URL)
                .set('Authorization', `Bearer ${API_HEADER}`)
                .expect(200);

            const apiResponse = res.body;
            await verifyEmptyAPIResponse(apiResponse);
        }

        await enrollTraveller(app, confirmationNumbers[0]);

        const res = await request(app.server())
            .get(URL)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyEmptyAPIResponse(apiResponse);

        await withdrawTraveller(app, confirmationNumbers[0]);
    });

    envCond('ADMIN')('returns withdrawn travellers with no date', async () => {
        const res = await request(app.server())
            .get(URL)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, 'withdrawn');
    });

    envCond('ADMIN')('returns withdrawn travellers with todays date', async () => {
        const res = await request(app.server())
            .get(URL)
            .query(`date=${moment().tz(USER_TIMEZONE).format(DATE_FORMAT)}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, 'withdrawn');
    });

    envCond('ADMIN')('returns withdrawn travellers with future date', async () => {
        const res = await request(app.server())
            .get(URL)
            .query(`date=${moment().tz(USER_TIMEZONE).add(1, 'day').format(DATE_FORMAT)}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyEmptyAPIResponse(apiResponse);
    });
});

describe('Analytics API retrieve withdrawn travellers - additional traveller', () => {
    let app;
    const URL = '/api/v2/rtop/analytics/withdrawn';
    let API_HEADER;

    beforeAll(async () => {
        app = await createTestingApp({ imports: [ConfigModule.forRoot()], entities: null, providers: null }, { disableAuth: true });
        await app.start();
        await ClearRTOPDB();
        API_HEADER = process.env.AHS_TEST_HEADER;
    });

    afterAll(async () => {
        await ClearRTOPDB();
        await app.close();
    });

    envCond('ADMIN')('withdraws travellers', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        await withdrawTraveller(app, confirmationNumbers[0]);

        const res = await request(app.server())
            .get(URL)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyEmptyAPIResponse(apiResponse);
    });

    envCond('ADMIN')('enrolls travellers', async () => {
        const confirmationNumbers = await getConfirmationNumbers(app, true);

        await enrollTraveller(app, confirmationNumbers[0]);

        const res = await request(app.server())
            .get(URL)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyEmptyAPIResponse(apiResponse);

    });

    envCond('ADMIN')('returns withdrawn travellers with previous date', async () => {
        const res = await request(app.server())
            .get(URL)
            .query(`date=${moment().tz(USER_TIMEZONE).subtract('1','day').format(DATE_FORMAT)}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyEmptyAPIResponse(apiResponse);

    });

    envCond('ADMIN')('returns withdrawn travellers with no date', async () => {
        
        const res = await request(app.server())
            .get(URL)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, 'withdrawn', true);
    });

    envCond('ADMIN')('returns withdrawn travellers with todays date', async () => {
        const res = await request(app.server())
            .get(URL)
            .query(`date=${moment().tz(USER_TIMEZONE).format(DATE_FORMAT)}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyAPIStructure(apiResponse, 'withdrawn', true);
    });

    envCond('ADMIN')('returns withdrawn travellers with future date', async () => {
        const res = await request(app.server())
            .get(URL)
            .query(`date=${moment().tz(USER_TIMEZONE).add(1, 'day').format(DATE_FORMAT)}`)
            .set('Authorization', `Bearer ${API_HEADER}`)
            .expect(200);

        const apiResponse = res.body;
        await verifyEmptyAPIResponse(apiResponse);
    });
});
import { createTestingApp } from "../util/db-test-utils";
import request from "supertest";
import {forms} from '../../db/seed-data/arrival-form-seed-data.json'
import { AuthModule } from "../../auth/auth.module";
import { ConfigModule } from "@nestjs/config";
import { AuthController } from "../../auth/auth.controller";
import { envCond } from "../util/test-utils";

const createFormEndpoint = '/api/v1/form';
const lookupByConfirmationNumberEndpoint = '/api/v1/admin/back-office/form';
const submitDeterminationEndpoint = '/api/v1/admin/back-office/form';
const findByLastNameEndpoint = '/api/v1/admin/back-office/lastname';
const validateAuthEndpoint = '/api/v1/auth/validate';

// InvalidJwt
const InvalidJwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.Ydn9P3cRQW63hVpxjudnIiAfACjTnh6FmAsM7qEBxhs';

let app;
let appContext;

afterEach(async () => {
  await app.close();
});

beforeEach(async () => {
  app = await createTestingApp({
    imports: [
      AuthModule,
      ConfigModule.forRoot()
    ],
    entities: null,
    providers: [AuthController],
  }, {disableAuth: false});

  appContext = await app.start();
});

describe('authentication', () => {
  describe('create endpoint', () => {
    envCond('ADMIN')('is public', async () => {
        await (await request.agent(app.server())
            .post(createFormEndpoint)
            .send(forms[0])
            .expect(201))
    });
  });

  describe('lookup by confirmation number endpoint', () => {
    envCond('ADMIN')('cannot be accessed with an invalid bearer token', async () => {
      await (await request.agent(app.server())
          .get(`${lookupByConfirmationNumberEndpoint}/ABTQNDPN`)
          .set('Authorization', `Bearer ${InvalidJwt}`)
          .set('authissuer', `AHS`)
          .expect(401))
    });

    envCond('ADMIN')('cannot be accessed with GOA credentials', async () => {
      await (await request.agent(app.server())
          .get(`${lookupByConfirmationNumberEndpoint}/ABTQNDPN`)
          .set('Authorization', `Bearer ${InvalidJwt}`)
          .set('authissuer', `GOA`)
          .expect(401));
    });

    envCond('ADMIN')('cannot be accessed without a bearer token', async () => {
      await (await request.agent(app.server())
          .get(`${lookupByConfirmationNumberEndpoint}/ABTQNDJN`)
          .set('authissuer', `OTHER`)
          .expect(401))
    });
  });

  describe('findByLastNameEndpoint', () => {
    envCond('ADMIN')('cannot be accessed without a bearer token', async () => {
      await (await request.agent(app.server())
          .get(`${findByLastNameEndpoint}/Smith`)
          .set('authissuer', `AHS`)
          .expect(401))
    });

    envCond('ADMIN')('cannot be accessed with an invalid bearer token', async () => {
      await (await request.agent(app.server())
          .get(`${findByLastNameEndpoint}/Smith`)
          .set('Authorization', `Bearer ${InvalidJwt}`)
          .set('authissuer', `OTHER`)
          .expect(401))
    });

    envCond('ADMIN')('cannot be accessed with GOA credentials', async () => {
      await (await request.agent(app.server())
          .get(`${findByLastNameEndpoint}/Smith`)
          .set('Authorization', `Bearer ${InvalidJwt}`)
          .set('authissuer', `GOA`)
            .expect(401))
    });
  });

  describe('submitDeterminationEndpoint', () => {
    envCond('ADMIN')('cannot be accessed without a bearer token', async () => {
      await (await request.agent(app.server())
          .patch(`${submitDeterminationEndpoint}/1/determination`)
          .set('authissuer', `AHS`)
          .send({determination: 'Accepted', notes: 'Looks good!'})
          .expect(401))
    });

    envCond('ADMIN')('cannot be accessed with an invalid bearer token', async () => {
      await (await request.agent(app.server())
          .patch(`${submitDeterminationEndpoint}/1/determination`)
          .set('Authorization', `Bearer ${InvalidJwt}`)
          .set('authissuer', `OTHER`)
          .send({determination: 'Accepted', notes: 'Looks good!'})
          .expect(401))
    });
  });

  describe('validateAuthEndpoint', () => {
    envCond('ADMIN')('cannot be accessed without a bearer token', async () => {
      await (await request.agent(app.server())
          .get(validateAuthEndpoint)
          .set('authissuer', `AHS`)
          .expect(401))
    });
    envCond('ADMIN')('cannot be accessed with an invalid bearer token', async () => {
      await (await request.agent(app.server())
          .get(validateAuthEndpoint)
          .set('authissuer', `OTHER`)
          .set('Authorization', `Bearer ${InvalidJwt}`)
          .expect(401))
    });
  });
});


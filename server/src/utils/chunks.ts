/**
 * Returns the given array split up into sub-arrays of size <size>
 * 
 * Example:
 * - chunks([a,b,c,d,e,f,g,h,i], 2) => [[a,b], [c,d], [e,f], [g,h], [i]]
 */
export const chunks = (array, size) => {
    if(size <= 0) {
      return [array];
    }

    const chunkArr = [];
    let idx = 0;
    while (idx < array.length) {
      chunkArr.push(array.slice(idx, size + idx));
      idx += size;
    }
    return chunkArr;
}
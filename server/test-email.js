var createTransport = require('nodemailer').createTransport;
var node_handlebars = require('nodemailer-express-handlebars')
var join = require('path').join;

/**
 * Utility script to test smtp connection.
 * 
 * Usage:
 * - TRANSPORT=... TEST_EMAIL=... npm run test-email-connection
 */

/** Transport (base64 encoded) {
*    host: '',
*    port: '',
*    secure: '',
*    auth: {
*        user: '',
*        pass: '',
*    }''
*}
 */

/** Email (base64 encoded) {
*    to: '',
*    from: '',
*    subject: '',
*    template: '',
*    context: ''
*}
 */

var transport = JSON.parse(Buffer.from(process.env.TRANSPORT, 'base64').toString());
var email = JSON.parse(Buffer.from(process.env.TEST_EMAIL, 'base64').toString());


this.transporter = createTransport(transport);

this.transporter.use(
    'compile',
    node_handlebars({
        viewEngine: {
            extname: '.hbs',
            layoutsDir: join(__dirname, 'src/email/templates/emails/'),
            partialsDir: join(__dirname, 'src/email/templates/partials/'),
        },
        viewPath: join(__dirname, 'src/email/templates/emails/'),
        extName: '.hbs',
    }),
);

this.transporter.sendMail(email, (error) => {
    if(error) {
        console.log('Failed to send mail');
        console.log(error);
    } else {
        console.log('Sent mail');
    }
});    

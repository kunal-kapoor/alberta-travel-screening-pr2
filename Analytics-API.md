
# Alberta Border Pilot analytics API

The Alberta Border Pilot analytics API is an API for retrieving information
about travellers participating in the Alberta Border Pilot program.

## Endpoints

API Host for dev environment: https://admin-dev.borderpilot.alberta.ca

API Host for prod environment: https://api.borderpilot.alberta.ca


### Authorization
All endpoints in the analytics API requires Bearer authentication to access. All endpoints expect an `Authorization` header containing a signed JWT token (`Authorization: Bearer <token>`). The `token` should be a `JWT` token signed with `RS256` (certificates will be provided for dev and prod environments).

### Retrieve enrolled travellers

`GET /api/v2/rtop/analytics/enrolled`

Returns travellers currently enrolled in the program.

### Retrieve withdrawn travellers

`GET /api/v2/rtop/analytics/withdrawn`

Returns all travellers currently withdrawn from the program.

### Retrieve daily check in answers

`GET /api/v2/rtop/analytics/daily-checkin`

Returns daily check-in responses.

### Request format
The endpoints above all optionally take in a `date` parameter to only return travellers enrolled/withdrawn or filled-in their daily checkins from 12 A.M to 11:59 P.M on that date.


`GET` Parameters:
- date: `ISO 8601` timestamp

Examples:

```
GET /api/v2/rtop/analytics/enrolled?date=2020-11-03T00:21:54Z
GET /api/v2/rtop/analytics/withdrawn?date=2020-11-03
GET /api/v2/rtop/analytics/daily-checkin?date=2020-11-03
```

### Return codes
- `200` OK
- `500` Something went wrong when retrieving travellers


## Example (using Nodejs)

### Generating auth token

Example on how to sign a JWT token using the provided certificate that can be
passed as a Bearer token when making requests to the API.

```js
import jwt from 'jsonwebtoken';

const privateKey = fs.readFileSync('./ab-dev-api-priv.pem');

const token = jwt.sign(
    {issuer: 'AB'},
    privateKey,
    {algorithm: 'RS256', expiresIn: '360s'}
);

console.log(token)
```

## Spec

### Traveller information

```json
{
  "travellers": [
    {
      "dateOfBirth": "YYYY-MM-DD",
      "gender": "Male|Female|Non-Binary|Transgender - M to F|Transgender - F to M|Two-Spirit|Another gender not listed|Prefer not to answer",
      "citizenshipStatus": "Citizen|Permanent resident|Foreign citizen",
      "exemptionType": "Exempt|Non-Exempt",
      "exemptOccupation": "Crew member (air)|Crew member (rail)|Trade or transportation sector, other (including truckers)|Provider of emergency services|Other",
      "exemptOccupationDetails": "Air transportation|Support activities for air transportation|Rail freight|Support activities for rail transportation|Local truck, long distance truck|Pipeline transportation|Postal service, couriers and messengers|Firefighter|Peace officer|Paramedic|Other",
      "hasAdditionalTravellers": "Yes|No",
      "hasTravellersOutsideHousehold": "Yes|No",
      "nameOfAirportOrBorderCrossing": "Airport - Calgary International (YYC)|Coutts Border Crossing",
      "arrivalDate": "YYYY-MM-DD",
      "arrivalCityOrTown": "string",
      "arrivalCountry": "string (Country Name)",
      "dateLeftCanada": "YYYY-MM-DD",
      "countryOfResidence": "string (Country Name)",
      "airline": "string",
      "flightNumber": "string",
      "seatNumber": "string",
      "reasonForTravel": "Work|Study|Personal|Returning Home",
      "durationOfStay": "Less than 14 days|14 days or more",
      "dateLeavingCanada": "YYYY-MM-DD",
      "address": "string",
      "cityOrTown": "string",
      "provinceTerritory": "Alberta|British Columbia|Manitoba|New Brunswick|Newfoundland and Labrador|Northwest Territories|Nova Scotia|Nunavut|Ontario|Prince Edward Island|Québec|Saskatchewan|Yukon|<free text>",
      "postalCode": "string",
      "typeOfPlace": "Private residence|Commercial (e.g. hotel)|<free text>",
      "howToGetToPlace": "Private vehicle|Taxi or rideshare|<free text>",
      "enrollmentStatus": "enrolled|withdrawn",
      "viableIsolationPlan": true|false,
      "created": "YYYY-MM-DDTHH:mm:ss.SZ",
      "id": "string",
      "enrollmentDate": "YYYY-MM-DD",
      "travellerStatus": "green",
      "lastUpdated": "YYYY-MM-DDTHH:mm:ss.SZ",
      "preDepartureTest": "Yes|No",
      "preDepartureVaccination": "Yes|No",
      "timeSinceVaccination": "string",
      "dosesOfVaccine": "string",
    },
    {
        ...
    },
    ...
  ]
}
```

### Daily check in

```json
{
   "travellers": [
      {
         "id": "string", (traveller id)
         "date": "YYYY-MM-DD",
         "submission": [
            {
               "question": "string",
               "answer": "yes|no"
            },
            {
               "question": "string",
               "answer": "yes|no"
            }
            ...
         ]
      },
      ...
   ]
}
```

## Sample responses

### Traveller information

```json
{
  "travellers": [
    {
      "dateOfBirth": "2020-09-01",
      "gender": "Male",
      "citizenshipStatus": "Permanent resident",
      "exemptionType": "Non-Exempt",
      "exemptOccupation": "",
      "exemptOccupationDetails": "",
      "hasAdditionalTravellers": "No",
      "hasTravellersOutsideHousehold": "No",
      "nameOfAirportOrBorderCrossing": "Coutts Border Crossing",
      "arrivalDate": "2020-11-02",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Australia",
      "dateLeftCanada": "",
      "countryOfResidence": "Antigua and Barbuda",
      "airline": "",
      "flightNumber": "",
      "seatNumber": "",
      "reasonForTravel": "",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "3223, ajnfj",
      "cityOrTown": "Airdrie",
      "provinceTerritory": "Manitoba",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "Private residence",
      "howToGetToPlace": "Private vehicle",
      "enrollmentStatus": "enrolled",
      "viableIsolationPlan": true,
      "created": "2020-11-02T17:18:34.113Z",
      "id": "AB8C238",
      "enrollmentDate": "2020-11-02",
      "travellerStatus": "green",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "Yes",
      "preDepartureVaccination": "No"
    },
    {
      "dateOfBirth": "2020-09-01",
      "gender": "Female",
      "citizenshipStatus": "Permanent resident",
      "exemptionType": "Non-Exempt",
      "exemptOccupation": "",
      "exemptOccupationDetails": "",
      "hasAdditionalTravellers": "No",
      "hasTravellersOutsideHousehold": "No",
      "nameOfAirportOrBorderCrossing": "Coutts Border Crossing",
      "arrivalDate": "2020-11-02",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Argentina",
      "dateLeftCanada": "",
      "countryOfResidence": "Angola",
      "airline": "",
      "flightNumber": "",
      "seatNumber": "",
      "reasonForTravel": "",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "1234, kuirubaa",
      "cityOrTown": "Telfordville",
      "provinceTerritory": "Alberta",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "mine",
      "howToGetToPlace": "mine",
      "enrollmentStatus": "enrolled",
      "viableIsolationPlan": true,
      "created": "2020-11-02T17:50:03.487Z",
      "id": "ABB8682",
      "enrollmentDate": "2020-11-02",
      "travellerStatus": "green",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "No",
      "preDepartureVaccination": "No"
    },
    {
      "dateOfBirth": "2020-10-15",
      "gender": "Female",
      "citizenshipStatus": "Citizen",
      "exemptionType": "Exempt",
      "exemptOccupation": "Crew member (rail)",
      "exemptOccupationDetails": "Rail freight",
      "hasAdditionalTravellers": "Yes",
      "hasTravellersOutsideHousehold": "Yes",
      "nameOfAirportOrBorderCrossing": "Coutts Border Crossing",
      "arrivalDate": "2020-11-02",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Armenia",
      "dateLeftCanada": "2019/11/02",
      "countryOfResidence": "Canada",
      "airline": "Air Canada",
      "flightNumber": "Ac122",
      "seatNumber": "23",
      "reasonForTravel": "Returning Home",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "1333 Souht park sst",
      "cityOrTown": "Banff",
      "provinceTerritory": "Nunavut",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "Private residence",
      "howToGetToPlace": "Private vehicle",
      "enrollmentStatus": "applied",
      "viableIsolationPlan": true,
      "created": "2020-11-02T21:05:56.364Z",
      "id": "AB9B83F",
      "travellerStatus": "green",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "",
      "preDepartureVaccination": ""
    },
    {
      "dateOfBirth": "2020-10-15",
      "gender": "Female",
      "citizenshipStatus": "Citizen",
      "exemptionType": "Exempt",
      "exemptOccupation": "Crew member (rail)",
      "exemptOccupationDetails": "Rail freight",
      "hasAdditionalTravellers": "Yes",
      "hasTravellersOutsideHousehold": "Yes",
      "nameOfAirportOrBorderCrossing": "Coutts Border Crossing",
      "arrivalDate": "2020-11-02",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Armenia",
      "dateLeftCanada": "2019/11/02",
      "countryOfResidence": "Canada",
      "airline": "Air Canada",
      "flightNumber": "Ac122",
      "seatNumber": "23",
      "reasonForTravel": "Returning Home",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "1333 Souht park sst",
      "cityOrTown": "Banff",
      "provinceTerritory": "Nunavut",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "Private residence",
      "howToGetToPlace": "Private vehicle",
      "enrollmentStatus": "applied",
      "viableIsolationPlan": true,
      "created": "2020-11-02T21:05:56.364Z",
      "id": "AB9B83F",
      "travellerStatus": "green",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "Yes",
      "preDepartureVaccination": "Yes",
      "timeSinceVaccination": "3-4 weeks",
      "dosesOfVaccine": "2"
    },
    {
      "dateOfBirth": "2020-11-02",
      "gender": "Non-Binary",
      "citizenshipStatus": "Permanent resident",
      "exemptionType": "Non-Exempt",
      "exemptOccupation": "",
      "exemptOccupationDetails": "",
      "hasAdditionalTravellers": "No",
      "hasTravellersOutsideHousehold": "No",
      "nameOfAirportOrBorderCrossing": "Coutts Border Crossing",
      "arrivalDate": "2020-11-02",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Angola",
      "dateLeftCanada": "",
      "countryOfResidence": "Albania",
      "airline": "",
      "flightNumber": "",
      "seatNumber": "",
      "reasonForTravel": "",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "133 sjbfs",
      "cityOrTown": "my city",
      "provinceTerritory": "Ontario",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "Commercial (e.g. hotel)",
      "howToGetToPlace": "Taxi or rideshare",
      "enrollmentStatus": "enrolled",
      "viableIsolationPlan": true,
      "created": "2020-11-03T01:40:51.587Z",
      "id": "AB8549A",
      "enrollmentDate": "2020-11-02",
      "travellerStatus": "green",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "",
      "preDepartureVaccination": ""
    },
    {
      "dateOfBirth": "2020-09-01",
      "gender": "Two-Spirit",
      "citizenshipStatus": "Foreign citizen",
      "exemptionType": "Non-Exempt",
      "exemptOccupation": "",
      "exemptOccupationDetails": "",
      "hasAdditionalTravellers": "No",
      "hasTravellersOutsideHousehold": "No",
      "nameOfAirportOrBorderCrossing": "Coutts Border Crossing",
      "arrivalDate": "2020-11-02",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Anguilla",
      "dateLeftCanada": "",
      "countryOfResidence": "Andorra",
      "airline": "",
      "flightNumber": "",
      "seatNumber": "",
      "reasonForTravel": "",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "asda kjasbdjk",
      "cityOrTown": "Airdrie",
      "provinceTerritory": "Nunavut",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "Commercial (e.g. hotel)",
      "howToGetToPlace": "Taxi or rideshare",
      "enrollmentStatus": "enrolled",
      "viableIsolationPlan": true,
      "created": "2020-11-03T01:43:15.961Z",
      "id": "AB81467",
      "enrollmentDate": "2020-11-02",
      "travellerStatus": "red",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "",
      "preDepartureVaccination": ""
    },
    {
      "dateOfBirth": "2020-10-01",
      "gender": "Female",
      "citizenshipStatus": "Permanent resident",
      "exemptionType": "Non-Exempt",
      "exemptOccupation": "",
      "exemptOccupationDetails": "",
      "hasAdditionalTravellers": "No",
      "hasTravellersOutsideHousehold": "No",
      "nameOfAirportOrBorderCrossing": "Coutts Border Crossing",
      "arrivalDate": "2020-11-02",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Aruba",
      "dateLeftCanada": "",
      "countryOfResidence": "Anguilla",
      "airline": "",
      "flightNumber": "",
      "seatNumber": "",
      "reasonForTravel": "",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "hsbdfhs",
      "cityOrTown": "Barrhead",
      "provinceTerritory": "Northwest Territories",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "Private residence",
      "howToGetToPlace": "Private vehicle",
      "enrollmentStatus": "enrolled",
      "viableIsolationPlan": true,
      "created": "2020-11-03T01:44:55.928Z",
      "id": "ABCE35F",
      "enrollmentDate": "2020-11-02",
      "travellerStatus": "red",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "",
      "preDepartureVaccination": ""
    },
    {
      "dateOfBirth": "2020-09-01",
      "gender": "Male",
      "citizenshipStatus": "Permanent resident",
      "exemptionType": "Non-Exempt",
      "exemptOccupation": "",
      "exemptOccupationDetails": "",
      "hasAdditionalTravellers": "Yes",
      "hasTravellersOutsideHousehold": "No",
      "nameOfAirportOrBorderCrossing": "Airport - Calgary International (YYC)",
      "arrivalDate": "2020-11-04",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Azerbaijan",
      "dateLeftCanada": "",
      "countryOfResidence": "Aruba",
      "airline": "",
      "flightNumber": "",
      "seatNumber": "",
      "reasonForTravel": "",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "2636, sdjfbuj sjdbfojsd",
      "cityOrTown": "Banff",
      "provinceTerritory": "Nova Scotia",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "Private residence",
      "howToGetToPlace": "Private vehicle",
      "enrollmentStatus": "enrolled",
      "viableIsolationPlan": true,
      "created": "2020-11-05T03:24:37.441Z",
      "id": "AB735D2",
      "enrollmentDate": "2020-11-04",
      "travellerStatus": "green",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "",
      "preDepartureVaccination": ""
    },
    {
      "dateOfBirth": "2020-09-01",
      "gender": "Male",
      "citizenshipStatus": "Permanent resident",
      "exemptionType": "Non-Exempt",
      "exemptOccupation": "",
      "exemptOccupationDetails": "",
      "hasAdditionalTravellers": "Yes",
      "hasTravellersOutsideHousehold": "No",
      "nameOfAirportOrBorderCrossing": "Airport - Calgary International (YYC)",
      "arrivalDate": "2020-11-04",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Azerbaijan",
      "dateLeftCanada": "",
      "countryOfResidence": "Aruba",
      "airline": "",
      "flightNumber": "",
      "seatNumber": "",
      "reasonForTravel": "",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "2636, sdjfbuj sjdbfojsd",
      "cityOrTown": "Banff",
      "provinceTerritory": "Nova Scotia",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "Private residence",
      "howToGetToPlace": "Private vehicle",
      "enrollmentStatus": "enrolled",
      "viableIsolationPlan": true,
      "created": "2020-11-05T03:24:37.441Z",
      "id": "AB735D2",
      "enrollmentDate": "2020-11-04",
      "travellerStatus": "green",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "",
      "preDepartureVaccination": ""
    },
    {
      "dateOfBirth": "2020-09-01",
      "gender": "Male",
      "citizenshipStatus": "Permanent resident",
      "exemptionType": "Non-Exempt",
      "exemptOccupation": "",
      "exemptOccupationDetails": "",
      "hasAdditionalTravellers": "Yes",
      "hasTravellersOutsideHousehold": "No",
      "nameOfAirportOrBorderCrossing": "Airport - Calgary International (YYC)",
      "arrivalDate": "2020-11-04",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Azerbaijan",
      "dateLeftCanada": "",
      "countryOfResidence": "Aruba",
      "airline": "",
      "flightNumber": "",
      "seatNumber": "",
      "reasonForTravel": "",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "2636, sdjfbuj sjdbfojsd",
      "cityOrTown": "Banff",
      "provinceTerritory": "Nova Scotia",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "Private residence",
      "howToGetToPlace": "Private vehicle",
      "enrollmentStatus": "enrolled",
      "viableIsolationPlan": true,
      "created": "2020-11-05T03:24:37.441Z",
      "id": "AB735D2",
      "enrollmentDate": "2020-11-04",
      "travellerStatus": "red",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "",
      "preDepartureVaccination": ""
    },
    {
      "dateOfBirth": "2020-09-01",
      "gender": "Male",
      "citizenshipStatus": "Permanent resident",
      "exemptionType": "Non-Exempt",
      "exemptOccupation": "",
      "exemptOccupationDetails": "",
      "hasAdditionalTravellers": "Yes",
      "hasTravellersOutsideHousehold": "No",
      "nameOfAirportOrBorderCrossing": "Airport - Calgary International (YYC)",
      "arrivalDate": "2020-11-04",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Azerbaijan",
      "dateLeftCanada": "",
      "countryOfResidence": "Aruba",
      "airline": "",
      "flightNumber": "",
      "seatNumber": "",
      "reasonForTravel": "",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "2636, sdjfbuj sjdbfojsd",
      "cityOrTown": "Banff",
      "provinceTerritory": "Nova Scotia",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "Private residence",
      "howToGetToPlace": "Private vehicle",
      "enrollmentStatus": "enrolled",
      "viableIsolationPlan": true,
      "created": "2020-11-05T03:24:37.441Z",
      "id": "AB735D2",
      "enrollmentDate": "2020-11-04",
      "travellerStatus": "red",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "",
      "preDepartureVaccination": ""
    },
    {
      "dateOfBirth": "2020-11-01",
      "gender": "Male",
      "citizenshipStatus": "Citizen",
      "exemptionType": "Non-Exempt",
      "exemptOccupation": "",
      "exemptOccupationDetails": "",
      "hasAdditionalTravellers": "No",
      "hasTravellersOutsideHousehold": "No",
      "nameOfAirportOrBorderCrossing": "Airport - Calgary International (YYC)",
      "arrivalDate": "2020-11-05",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Iceland",
      "dateLeftCanada": "",
      "countryOfResidence": "Australia",
      "airline": "",
      "flightNumber": "",
      "seatNumber": "",
      "reasonForTravel": "",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "1333 South Park St",
      "cityOrTown": "Banff",
      "provinceTerritory": "Alberta",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "Private residence",
      "howToGetToPlace": "Private vehicle",
      "enrollmentStatus": "applied",
      "viableIsolationPlan": true,
      "created": "2020-11-05T20:17:37.468Z",
      "id": "ABCFEEC",
      "travellerStatus": "green",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "",
      "preDepartureVaccination": ""
    },
    {
      "dateOfBirth": "2020-11-01",
      "gender": "Male",
      "citizenshipStatus": "Foreign citizen",
      "exemptionType": "Exempt",
      "exemptOccupation": "Crew member (rail)",
      "exemptOccupationDetails": "Rail freight",
      "hasAdditionalTravellers": "Yes",
      "hasTravellersOutsideHousehold": "Yes",
      "nameOfAirportOrBorderCrossing": "Coutts Border Crossing",
      "arrivalDate": "2020-11-05",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Azerbaijan",
      "dateLeftCanada": "",
      "countryOfResidence": "Canada",
      "airline": "",
      "flightNumber": "",
      "seatNumber": "",
      "reasonForTravel": "",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "my address",
      "cityOrTown": "Banff",
      "provinceTerritory": "Alberta",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "Private residence",
      "howToGetToPlace": "Private vehicle",
      "enrollmentStatus": "applied",
      "viableIsolationPlan": true,
      "created": "2020-11-05T23:12:46.317Z",
      "id": "AB01D42",
      "travellerStatus": "red",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "",
      "preDepartureVaccination": ""
    },
    {
      "dateOfBirth": "2020-11-01",
      "gender": "Male",
      "citizenshipStatus": "Foreign citizen",
      "exemptionType": "Exempt",
      "exemptOccupation": "Crew member (rail)",
      "exemptOccupationDetails": "Rail freight",
      "hasAdditionalTravellers": "Yes",
      "hasTravellersOutsideHousehold": "Yes",
      "nameOfAirportOrBorderCrossing": "Coutts Border Crossing",
      "arrivalDate": "2020-11-05",
      "arrivalCityOrTown": "Vic",
      "arrivalCountry": "Azerbaijan",
      "dateLeftCanada": "",
      "countryOfResidence": "Canada",
      "airline": "",
      "flightNumber": "",
      "seatNumber": "",
      "reasonForTravel": "",
      "durationOfStay": "14 days or more",
      "dateLeavingCanada": "",
      "address": "my address",
      "cityOrTown": "Banff",
      "provinceTerritory": "Alberta",
      "postalCode": "V3N 4R8",
      "typeOfPlace": "Private residence",
      "howToGetToPlace": "Private vehicle",
      "enrollmentStatus": "applied",
      "viableIsolationPlan": true,
      "created": "2020-11-05T23:12:46.317Z",
      "id": "AB01D42",
      "travellerStatus": "red",
      "lastUpdated": "2020-11-06T19:27:40Z",
      "preDepartureTest": "",
      "preDepartureVaccination": ""
    }
  ]
}
```

### Daily Check-In


```json
{
   "travellers": [
      {
         "id": "AB9E25F-0",
         "date": "2020-10-12",
         "submission": [
            {
               "question": "new or worsening cough",
               "answer": "yes"
            },
            {
               "question": "shortness of breath/difficulty breathing",
               "answer": "yes"
            },
            {
               "question": "temperature equal to or over 38°C",
               "answer": "no"
            },
            {
               "question": "feeling feverish, chills, fatigue or weakness",
               "answer": "no"
            },
            {
               "question": "muscle or body aches",
               "answer": "no"
            },
            {
               "question": "new loss of smell or taste",
               "answer": "no"
            },
            {
               "question": "headache",
               "answer": "no"
            },
            {
               "question": "gastrointestinal symptoms like abdominal pain, diarrhea, vomiting, or feeling very unwell",
               "answer": "no"
            }
         ]
      },
      {
         "id": "AB9E25F-1",
         "date": "2020-10-12",
         "submission": [
            {
               "question": "new or worsening cough",
               "answer": "yes"
            },
            {
               "question": "shortness of breath/difficulty breathing",
               "answer": "no"
            },
            {
               "question": "temperature equal to or over 38°C",
               "answer": "no"
            },
            {
               "question": "feeling feverish, chills, fatigue or weakness",
               "answer": "no"
            },
            {
               "question": "muscle or body aches",
               "answer": "no"
            },
            {
               "question": "new loss of smell or taste",
               "answer": "no"
            },
            {
               "question": "headache",
               "answer": "no"
            },
            {
               "question": "gastrointestinal symptoms like abdominal pain, diarrhea, vomiting, or feeling very unwell",
               "answer": "no"
            }
         ]
      },
      {
         "id": "AB9E24F-0",
         "date": "2020-10-12",
         "submission": [
            {
               "question": "new or worsening cough",
               "answer": "no"
            },
            {
               "question": "shortness of breath/difficulty breathing",
               "answer": "no"
            },
            {
               "question": "temperature equal to or over 38°C",
               "answer": "no"
            },
            {
               "question": "feeling feverish, chills, fatigue or weakness",
               "answer": "no"
            },
            {
               "question": "muscle or body aches",
               "answer": "no"
            },
            {
               "question": "new loss of smell or taste",
               "answer": "no"
            },
            {
               "question": "headache",
               "answer": "no"
            },
            {
               "question": "gastrointestinal symptoms like abdominal pain, diarrhea, vomiting, or feeling very unwell",
               "answer": "no"
            }
         ]
      },
      {
         "id": "AB9E27F-0",
         "date": "2020-10-12",
         "submission": [
            {
               "question": "new or worsening cough",
               "answer": "yes"
            },
            {
               "question": "shortness of breath/difficulty breathing",
               "answer": "yes"
            },
            {
               "question": "temperature equal to or over 38°C",
               "answer": "no"
            },
            {
               "question": "feeling feverish, chills, fatigue or weakness",
               "answer": "no"
            },
            {
               "question": "muscle or body aches",
               "answer": "no"
            },
            {
               "question": "new loss of smell or taste",
               "answer": "no"
            },
            {
               "question": "headache",
               "answer": "no"
            },
            {
               "question": "gastrointestinal symptoms like abdominal pain, diarrhea, vomiting, or feeling very unwell",
               "answer": "no"
            }
         ]
      }
   ]
}
```
